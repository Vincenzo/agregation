function disque_unite1()
    x=2*rand()-1
    y=2*rand()-1
    while x^2+y^2>1
        x=2*rand()-1
        y=2*rand()-1
    end
    clf
    t=[0:2*%pi/100:100]
    a=gca();
    a.isoview='on';
    plot(cos(t),sin(t))
    plot(x,y,'ro')
endfunction

disque_unite1()