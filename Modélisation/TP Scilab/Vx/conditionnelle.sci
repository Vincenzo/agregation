function X=conditionnelle()
    X=grand(1,1,'exp',1);
    Y=grand(1,1,'exp',1);
    while X+2*Y<5
        X=grand(1,1,'exp',1);
        Y=grand(1,1,'exp',1);
    end
endfunction

x=[];
for i=1:1000
    x=[x,conditionnelle()];
end
mean(x)
scf(0)
clf()
plot(x)
plot(1:1000,mean(x),'r-')