function res=rand_cauchy(m,n)
    res=tan(%pi/2*(2*rand(m,n)-1))
endfunction

rand_cauchy(1,10)
