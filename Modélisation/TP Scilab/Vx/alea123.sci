function res=alea123()
    funcprot(0)
    rand('seed',getdate('s'))
    x=rand()
    if x>0.4 then
        res=3
    elseif 0.3<x & x<0.4 then
        res=2
    elseif x<0.3 then
        res=1
    end
endfunction

alea123()