// Estimation de la probabilité d'erreur

n=100; // taille de l'échantillon
r=0.1; // pas de la règle

nb=1000; // nb de simulations

nb_hist=0; // nb d'erreurs de classification
nb_naif=0;
nb_gauss=0;

for i_simul=1:nb
	// tirage des points donnés
	Xi=[];Yi=[];
	for i=1:n
		[X,Y]=point_alea();
		Xi=[Xi,X]; Yi=[Yi,Y];
	end

	[X,Y]=point_alea();

	Y_hist=classif_histo(X,Xi,Yi,r); // calcul de Y selon règle de l'histogramme
	if(Y_hist~=Y)
		nb_hist=nb_hist+1
	end

	Y_naif=classif_noyau(X,Xi,Yi,r,K_naif); // calcul de Y selon noyau naïf
	if(Y_naif~=Y)
		nb_naif=nb_naif+1
	end

	Y_gauss=classif_noyau(X,Xi,Yi,r,K_gauss); // calcul de Y selon noyau gaussien
	if(Y_hist~=Y)
		nb_gauss=nb_gauss+1
	end
end

nb_hist=nb_hist/nb;
nb_naif=nb_naif/nb;
nb_gauss=nb_gauss/nb;

disp('Erreur histogramme : ');disp(nb_hist)
disp('Erreur naïf : ');disp(nb_naif)
disp('Erreur Gauss : ');disp(nb_gauss)
