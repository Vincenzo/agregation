// Tirage de n points, et affichage dans la fenêtre 0

n=100; // nombre de points donnés

// tirage des points donnés
Xi=[];Yi=[];
for i=1:n
	[X,Y]=point_alea();
	Xi=[Xi,X]; Yi=[Yi,Y];
end

// affichage dans la fenêtre 0
scf(0);clf
//for i=1:n
//	if Yi(i)==1
//		plot(Xi(1,i),Xi(2,i),'.r') // rouge si 1
//	else
//		plot(Xi(1,i),Xi(2,i),'.b') // bleu si 0
//	end
//end
//
I=(Yi==1);
plot(Xi(1,I),Xi(2,I),'.r');
plot(Xi(1,~I),Xi(2,~I),'.b');
