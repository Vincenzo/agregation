// Estimation de la probabilité d'erreur

function [p_hist,p_naif,p_gauss]=estim_erreur(n,r,nb)
	nb_hist=0; // nb d'erreurs de classification
	nb_naif=0;
	nb_gauss=0;
	
	for i_simul=1:nb
		// tirage des points donnés
		Xi=[];Yi=[];
		for i=1:n
			[X,Y]=point_alea();
			Xi=[Xi,X]; Yi=[Yi,Y];
		end
	
		[X,Y]=point_alea();
	
		Y_hist=classif_histo(X,Xi,Yi,r); // calcul de Y selon règle de l'histogramme
		if(Y_hist~=Y)
			nb_hist=nb_hist+1
		end
	
		Y_naif=classif_noyau(X,Xi,Yi,r,K_naif); // calcul de Y selon noyau naïf
		if(Y_naif~=Y)
			nb_naif=nb_naif+1
		end
	
		Y_gauss=classif_noyau(X,Xi,Yi,r,K_gauss); // calcul de Y selon noyau gaussien
		if(Y_gauss~=Y)
			nb_gauss=nb_gauss+1
		end
	end
	
	p_hist=nb_hist/nb;
	p_naif=nb_naif/nb;
	p_gauss=nb_gauss/nb;
endfunction


n=100; // taille de l'échantillon

rs=linspace(0.05,1,10); // valeurs du pas considérées

nb=1000; // nb de simulations

ps_hist=[];ps_naif=[];ps_gauss=[];

for r=rs
	[ph,pn,pg]=estim_erreur(n,r,nb);
	ps_hist=[ps_hist,ph];
	ps_naif=[ps_naif,pn];
	ps_gauss=[ps_gauss,pg];
end

scf(1);clf;
plot(rs,ps_hist,'r');
plot(rs,ps_naif,'b');
plot(rs,ps_gauss,'g');
xtitle('Probabilités d''erreur selon le pas (n=100)','$h$','$p_{erreur}$')
legend(['histogramme','naïf','Gauss'])




