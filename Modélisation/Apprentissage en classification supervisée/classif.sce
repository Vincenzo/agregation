// Règles de classification

function Y=classif_histo(X,Xi,Yi,r)
// Applique la règle de l'histogramme de pas r à X (en colonne), 
// à l'aide des données Xi=[X1,X2,...] et Yi=[Y1,Y2,...]

	// Limites du carré contenant X
	x1min=floor(X(1)/r)*r;
	x1max=x1min+r;
	x2min=floor(X(2)/r)*r;
	x2max=x2min+r;

	// vecteurs des positions des points dans le carré [%T %T %F ...]
	I=(Xi(1,:)>x1min)&(Xi(1,:)<x1max)&(Xi(2,:)>x2min)&(Xi(2,:)<x2max)

	if(2*sum(Yi(I))>sum(I))
		Y=1
	else
		Y=0
	end
	
endfunction

function Y=classif_noyau(X,Xi,Yi,h,K)
// Applique la règle du noyau K de largeur h à X (en colonne), 
// à l'aide des données Xi=[X1,X2,...] et Yi=[Y1,Y2,...]

	S0=0;S1=0;
	for i=1:length(Yi)
		if(Yi(i)==1)
			S1=S1+K((Xi(:,i)-X)/h)
		else
			S0=S0+K((Xi(:,i)-X)/h)
		end
	end
	
	if(S1>S0)
		Y=1
	else
		Y=0
	end
endfunction

function z=K_gauss(X)
	z=exp(-X(1)^2-X(2)^2)
endfunction

function z=K_naif(X)
	if(norm(X)<1)
		z=1
	else
		z=0
	end
endfunction

function z=K_epan(X)
	z=(1-X(1)^2-X(2)^2);
	z=z*(z>0)
endfunction

function z=K_cauchy(X)
	z=1/(1+X(1)^2+X(2)^2)
endfunction
