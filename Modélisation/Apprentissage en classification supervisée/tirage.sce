function [X,Y]=point_alea()
// Renvoie un couple (X,Y) aléatoire
// X est de taille (2,1), et Y est 0 ou 1

	if(rand()<0.3)
		Y=1
		X=[.2+rand();rand()]
	else
		Y=0
		X=[rand();0.5+1.2*rand()]
		//X=grand(2,1,"nor",0,2)
	end

endfunction

// Ici, X est uniforme dans un carré qui dépend de Y
// Variantes : 
// X=grand(2,1,"nor",0,1)
// X=[1;0]+grand(2,1,"nor",0,2)
