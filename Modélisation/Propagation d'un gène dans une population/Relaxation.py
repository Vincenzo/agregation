#!/usr/bin/python

import numpy as np
import matplotlib.pyplot as plt

n = 25

s = np.zeros(n)
s[n-1] = 1
S = np.zeros_like(s)

for k in range(1000):
    for i in range(3,n-3):
        S[i] = (s[i-2] + s[i-1] + s[i] + s[i+1] + s[i+2])/5
    S[1] = s[1]
    S[n-1] = s[n-1]
    S[2] = (2*s[1] + s[2] + s[3] + s[4])/5
    S[n-2] = (2*s[n-1] + s[n-2] + s[n-3] + s[n-4])/5

    s=S

plt.plot(S)

t = np.zeros(n)
t[n-1] = 1
T = np.zeros_like(s)

for k in range(1000):
    for i in range(3,n-3):
        t[i] = (t[i-2]/2 + t[i-1] + 2*t[i] + t[i+1] + t[i+2]/2)/5
    T[1] = t[1]
    T[n-1] = t[n-1]
    T[2] = (3/2*t[1] + 2*t[2] + t[3] + 1/2*t[4])/5
    T[n-2] = (3/2*t[n-1] + 2*t[n-2] + t[n-3] + 1/2*t[n-4])/5

    t=T

plt.hold(True)

plt.plot(T,'red')
plt.show()
