//n nombre de plants dans l'échantillon prélevé
n=15;

function c=ncr(n,p)
    c=factorial(n)/(factorial(p)*factorial(n-p))
endfunction

//Initialisons la liste pour itérer par relaxation
s=zeros(0,n); s(n)=1;S=zeros(0,n)
//vecteur de coefficients binomiaux
C=[]
for i=1:n
    C=[C,ncr(n,i)];
end

//Initialisons également un premier vecteur et itérons (par relaxation)
for k=1:100
    for i=3:n-2
        S(i)=(s(i-2)+s(i-1)+s(i)+s(i+1)+s(i+2))/5;
    end
    S(1)=s(1);S(n)=s(n);
    S(2)=(2*s(1)+s(2)+s(3)+s(4))/5;
    S(n-1)=(2*s(n)+s(n-1)+s(n-2)+s(n-3))/5;

    s=S;
end
clf(0);
plot(S);

//Initialisons la liste pour itérer par relaxation
t=zeros(0,n); t(n)=1;T=zeros(0,n)
//Cette fois on va utiliser des poids

for k=1:1000
    for i=3:n-2
        T(i)=t(i-2)/10+t(i-1)/5+2*t(i)/5+t(i+1)/5+t(i+2)/10;
    end
    T(1)=t(1);T(n)=t(n);
    T(2)=3*t(1)/10+2*t(2)/5+t(3)/5+t(4)/10;
    T(n-1)=3*t(n)/10+2*t(n-1)/5+t(n-2)/5+t(n-3)/10;

    t=T;
end
plot(T,'red');
