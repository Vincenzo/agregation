// Estimation de mu et alpha

n_max=1000; // nombre de sinistres

// Simulation des sinistres (temps et coûts)
mu=0.7;
intertemps=grand(1,n_max,'exp',1/mu); // intervalles entre sinistres
T=cumsum(intertemps); // instants des sinistres

lambda=3/mu; // donc alpha=3
X=grand(1,n_max,'exp',lambda); // coûts des sinistres
C=cumsum(X); // coût cumulé des sinistres

// On suppose désormais T et C connus, et on veut estimer mu et alpha=lambda*mu

disp('Estimation de mu : ')
mu_est=n_max/T($); // T($) : dernier élément de T
disp(mu_est)

disp('Intervalle de confiance pour mu, de niveau (asymptotique) 95% :')
d=sqrt(mu_est)/sqrt(T($));
disp('['+string(mu_est-1.96*d)+' , '+string(mu_est+1.96*d)+']')


disp('Estimation de alpha : ')
alpha_est=C($)/T($);
disp(alpha_est)

disp('Estimation de a : ')
a_est=variance(X)+mean(X)^2; // a=E[X^2]=Var(X)+E[X]^2 (=2*lambda^2 dans le cas exponentiel)
disp(a_est)

disp('Intervalle de confiance pour alpha, de niveau (asymptotique) 95% :')
d=sqrt(mu_est*a_est)/sqrt(T($));
disp('['+string(alpha_est-1.96*d)+' , '+string(alpha_est+1.96*d)+']')


// On fait un graphe des estimations (et intervalles de confiance) obtenus à partir des n premiers sinistres, quand n varie
mus=[];ic1=[];ic2=[]; // vecteurs des estimations et bornes des intervalles de confiance de mu
alphas=[];ica1=[];ica2=[]; // idem pour alpha
ns=10:n_max; // valeurs de n (on enlève les premières, qui donnent des valeurs non pertinentes)
for n=ns
  mu_est=n/T(n);
  mus=[mus,mu_est];
  d=sqrt(mu_est)/sqrt(n);
  ic1=[ic1, mu_est-1.96*d];
  ic2=[ic2, mu_est+1.96*d];
  
  alpha_est=C(n)/T(n);
  alphas=[alphas,alpha_est];
  a_est=variance(X(1:n))+mean(X(1:n))^2; // (pas optimal, algorithmiquement...)
  d=sqrt(mu_est*a_est)/sqrt(T(n));
  ica1=[ica1, alpha_est-1.96*d];
  ica2=[ica2, alpha_est+1.96*d];
end

scf(0);clf;
subplot(2,1,1);
plot(ns,mus,'b');
plot(ns,[ic1;ic2],'r');
plot(ns,mu,'k');
xtitle('Estimation de mu à partir de n sinistres, et intervalle de confiance (à n fixé)','n','mu_estim');

subplot(2,1,2);
plot(ns,alphas,'b');
plot(ns,[ica1;ica2],'r');
plot(ns,mu*lambda,'k');
xtitle('Estimation de alpha à partir de n sinistres, et intervalle de confiance (à n fixé)','n','alpha_estim');


