function [X,Y]=point_alea()
  // renvoie un couple (X,Y) aléatoire
  // X est de taille (2,1) et Y est 0 ou 1

  if(rand()<0.3)
    Y=1
    X=[.2+rand();rand()]
  else
    Y=0
    X=[rand();.5+1.2*rand()]
  end
endfunction
