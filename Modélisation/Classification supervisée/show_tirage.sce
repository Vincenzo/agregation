n=100

Xi=[];Yi=[];

for i=1:n
  [X,Y] = point_alea() ;
  Xi = [Xi,X] ; Yi = [Yi,Y] ;
end

//scf(0) ; clf
scf(0);clf;

I=(Yi==1);

plot(Xi(1,I),Xi(2,I),'.r');
plot(Xi(1,~I),Xi(2,~I),'.b');
