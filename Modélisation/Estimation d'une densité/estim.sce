function f=estim_rosenblatt(X,h,t)
// X vecteur ligne des données
// h largeur de fenêtre
// t point(s) d'évaluation (vecteur ligne)
	n=length(X);
	r=length(t);
	
	f=zeros(1,r);
	for i=1:r
		f(i)=sum(abs(X-t(i))<h);
	end
	
	f=f/(2*h*n);
endfunction

function f=estim_epan(X,h,t)
// X vecteur ligne des données
// h largeur de fenêtre
// t point(s) d'évaluation (vecteur ligne)
	n=length(X);
	
	f=[];
	for ti=t
		f=[f,sum((abs(X-ti)<h).*(1-(X-ti).^2/h^2))];
	end
		
	f=f/(h*n)*3/4;
endfunction

function f=estim_gauss(X,h,t)
// X vecteur ligne des données
// h largeur de fenêtre
// t point(s) d'évaluation (vecteur ligne)
	n=length(X);
	
	f=[];
	for ti=t
		f=[f,sum(exp(-((X-ti).^2/(2*h^2))))];
	end
	
	f=f/(h*n*sqrt(2*%pi));
endfunction


// *******************************************************
// Autre écriture (version fonctionnelle)
// On prend le noyau K en argument

function f=estim_densite(K,X,h,t)
// K fonction noyau, qui peut prendre comme argument un vecteur
// X vecteur ligne des données
// h largeur de fenêtre
// t point(s) d'évaluation (vecteur ligne)
	n=length(X);
	
	f=[];
	for ti=t
		f=[f,sum(K((X-ti)/h))];
	end
	
	f=f/n;
endfunction

// Fonctions noyaux :

// Noyau de Rosenblatt
// NB : si X=[x_1 ... x_n], la fonction renvoie [K(x_1) ... K(x_n)]
function a=K_Rosenblatt(X)
	a=(abs(X)<1)/2;
endfunction

// Noyau d'Epanechnikov
function a=K_Epan(X)
	a=(abs(X)<1).*(1-X.^2);
endfunction

// Noyau de Gauss
function a=K_Gauss(X)
	a=exp(X.^2/2)/sqrt(2*%pi);
endfunction

// Exemple de programme avec la version fonctionnelle :
//
// clf;
// X=rand(1,1000); 
// t=-0.5:0.01:1.5;
// plot(t,estim_densite(K_Epan,X,0.1,t),'r')


