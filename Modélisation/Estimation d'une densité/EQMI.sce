function e=EQMI(fth,fest,dt)
// ** Estimation de EQMI par méthode de Monte-Carlo
// fth : densité théorique (vecteur ligne)
// fest : densités empiriques (matrice : lignes i.i.d.)
// dt : largeur de l'intervalle entre les points d'évaluation

e=sum((ones(size(fest,1),1)*fth-fest).^2)*dt/size(fest,1);

endfunction

function es=EQMI_h(theor,simul,r,estim,n,hs,ts,dt)
// ** Calcule, pour plusieurs valeurs de h, l'EQMI **
// theor : fonction de densité théorique (fonction)
// simul : fonction de simulation
// r : taille de chaque échantillon pour estimation de densité
// estim : fonction d'estimation de densité
// n : nombre de simulations pour estimation de l'EQM
// hs : valeurs de h
// ts : valeurs de t où on estime la densité
// dt : écart entre ces valeurs de t
	es=[];
	fest=[];
	fth=theor(ts);
	for h=hs
		for i=1:n
			X=simul(r);
			fest=[fest;estim(X,h,ts)];
		end
		es=[es,EQMI(fth,fest,dt)];
	end
endfunction

function i=theorunif(t)
	i=(t>0)&(t<1);
endfunction

function x=simulunif(r)
	x=rand(1,r);
endfunction

hs=0.1:0.05:2;
es=EQMI_h(theorunif,simulunif,10,estim_epan,10,hs,-1:0.01:2,0.01);

scf(5);clf;
plot(hs,es,'r');
xtitle('EQMI en fonction de h (pour loi uniforme, avec noyau d''Epan., pour 10 points)','h','EQMI')
