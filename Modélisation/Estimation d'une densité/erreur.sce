// Calcul de l'erreur quadratique entre la densité estimée et la vraie densité, selon h, selon le noyau, selon la densité, selon n...
// On n'utilise pas de méthode de Monte-Carlo : la distance est calculée pour un seul échantillon, et est donc aléatoire. Néanmoins la simulation permet de voir que la valeur minimale fluctue peu selon la simulation (lancer le programme plusieurs fois).


n=1000;
X=rand(1,n); // échantillon (loi uniforme sur [0,1])

// Fonction densité de la loi uniforme sur [0,1]
function y=f_uniforme(x)
	y=(abs(x-0.5)<0.5)*1; // le "*1" sert à avoir un réel et non un booléen
endfunction


t=-0.5:0.01:1.5; // points d'évaluation de la densité
dt=t(2)-t(1); // pas de la subdivision

h=0.01:0.005:0.3; // valeurs de h à considérer

distances=[]; // vecteur qui contiendra la liste des distances pour les différents h

f=f_uniforme(t); // vraie densité, aux points donnés
for hi=h
	distances=[distances, norm(f-estim_rosenblatt(X,hi,t))*dt];
end

scf(7);clf;
plot(h,distances,'b');
xtitle('Distance L2 entre estimateur et densité, pour la loi uniforme et le noyau de R.','h','dist')

//*************
// Idem pour la loi normale


n=1000;
X=grand(1,n,'nor',0,1); // échantillon (loi normale(0,1))

// Fonction densité de la loi N(0,1)
function y=f_gauss(x)
	y=exp(-x.^2/2)/sqrt(2*%pi);
endfunction


t=-0.5:0.01:1.5; // points d'évaluation de la densité

h=0.01:0.01:1; // valeurs de h à considérer

distances=[];

f=f_gauss(t); // vraie densité, aux points donnés
for hi=h
	distances=[distances, norm(f-estim_rosenblatt(X,hi,t))*dt];
end

scf(8);clf;
plot(h,distances,'b');
xtitle('Distance L2 entre estimateur et densité, pour la loi N(0,1) et le noyau de R.','h','dist')


