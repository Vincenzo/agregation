# coding: utf-8
import numpy as np
import numpy.random as random
import matplotlib.pyplot as plt


n=100
x = np.array([ random.random() for i in range(n) ])
#x = np.array([ random.normal(0,1) for i in range(n) ])


def indicatrice(X,b=np.PINF,a=np.NINF):
    return [int(a < X[i] < b) for i in range(len(X))]

def histo(nb_classe,X):
    m = X.min()
    M = X.max()
    ampl = (M-m)/nb_classe
    Y = [sum(indicatrice(X,m+(i+1)*ampl,m+i*ampl)) for i in range(nb_classe)]
    xx = [ m + i*ampl for i in range(nb_classe) ]
    plt.plot(xx,Y)
    plt.show()
    return xx,Y

histo(10,x)

h=0.01
#h=n**(-1./5)

def Rosenblatt2(X,x,h):
    return 1/2*sum(indicatrice((X-x)/h,1,-1))/(h*len(X))

xx = np.arange(-1,1,0.02)
yy = np.array([Rosenblatt2(x,xx[i],(i+1)**(-1./5)) for i in range(n)])

def Epanechnikov(X,x,h):
    return 3/4*(1-x**2)*sum(indicatrice((X-x)/h,1,-1))/(h*len(X))

zz = np.array([Epanechnikov(x,xx[i],h) for i in range(n)])

plt.plot(xx,yy)
plt.plot(xx,zz,'r--')
plt.show()


