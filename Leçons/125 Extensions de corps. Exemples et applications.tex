\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\parindent0pt

\usepackage{babel}

\title{124 -- Extension de corps. Exemples et applications}
\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\section{Généralités}

\begin{definition}
  On appelle \emph{corps} tout anneau non nul dans le quel tous les
  éléments non nuls sont inversibles.
\end{definition}

\begin{proposition}
  \begin{itemize}
    \item $\mathbf{K}\setminus\{0\}$ est un groupe multiplicatif, en
      l'occurrence le groupe des éléments inversibles de l'anneau.
    \item $\mathbf{K}$ n'admet pas de diviseurs de 0.
    \item La caractéristique de $\mathbf{K}$ est 0 ou un nombre $p$
      premier (au titre d'anneau non nul sans diviseurs de 0)
    \item Si $a\neq 0$, l'équation $ax = b$ (resp. $xa = b$) admet
      l'unique solution $a^{-1}b$ (resp. $ba^{-1}$)
  \end{itemize}
\end{proposition}
\begin{proof}
  Démonstrations identiques à celles dans les anneaux ou dans les
  groupes.
\end{proof}

\begin{remarque}[Notation]
  Dans un corps commutatif, on note $a^{-1}b = \frac{b}a$.
\end{remarque}

\begin{definition}
  On appelle \emph{morphisme de coprs} tout morphisme des anneaux
  sous-jacents.
\end{definition}

\begin{definition}
  On appelle \emph{sous-corps} d'un corps $\mathbf{K}$ tout sous-anneau
  $\mathbf{L} \subset \mathbf{K}$ qui est un corps ; on dit alors que
  $\mathbf{K}$ est un \emph{sur-corps} de $\mathbf{L}$
\end{definition}

\begin{theoreme}[Wedderburn]
  Tout corps fini est commutatif
\end{theoreme}
\begin{proof}
  Développement
\end{proof}

\section{Extension de corps}

\begin{definition}
  Soit $\mathbf{L}$ un sous-corps de $\mathbf{K}$ et $\alpha \in
  \mathbf{K}$. On note $\mathbf{L}(\alpha)$ le sous-corps engendré par
  $\mathbf{L} \cup \{\alpha \}$. On l'appelle \emph{extension simple} de
  $\mathbf{L}$.
\end{definition}


\begin{definition}
  Corps des fractions sur un anneau intègre.
\end{definition}

\begin{proposition}
  Si $\alpha \in \mathbf{L}$ alors $\mathbf{L}(\alpha) = \mathbf{L}$.
\end{proposition}

\begin{exemples}
  \begin{itemize}
    \item $\mathbf{C} = \mathbf{R}(i)$
    \item $\mathbf{Q}(\sqrt{2})$
  \end{itemize}
\end{exemples}

Dans le cas des extensions simples, on considère le morphisme d'anneaux
$\varphi : \mathbf{K}[X] \to \mathbf{K}(a),\ P \mapsto P(a)$. Son image
est un sous-anneau noté $\mathbf{K}[a]$ de $\mathbf{K}(a)$. Ce noyau
image est isomorphe à $\mathbf{K}[X]/I$ où $I$ est l'idéal $\ker
\varphi$.

\begin{definition}
  L'élément $a$ est dit transcendant sur $\mathbf{K}$, ou encore
  l'extension $\mathbf{K}(a)$ est dite transcendante, si et seulement si
  $I = \{0\}$, ce qui se dit aussi que le seul polynome sur $\mathbf{K}$
  qui annule $a$ est le polynome nul.
\end{definition}

\begin{proposition}
  Si $a$ est transcendant sur $\mathbf{K}$, le corps $\mathbf{K}(a)$ est
  le corps des fractions de l'anneau intègre $\mathbf{K}[a]$; il est
  isomorphe au corps $\mathbf{K}(X)$ des fractions rationnelles à une
  indéterminée sur $\mathbf{K}$.
\end{proposition}
\begin{proof}
  Si $I = \{0\}$ alors $\mathbf{K}[a]$ est isomorphe à $\mathbf{K}[X]$
  et donc on retrouve que $\mathbf{K}[a]$ est intègre. $\mathbf{K}(a)$
  est le plus petit corps contenant $a$ et $\mathbf{K}$ et donc
  $\mathbf{K}[a]$, il s'agit donc du corps des fractions de
  $\mathbf{K}[a]$ et on peut prolonger l'isomorphisme de $\mathbf{K}[X]$
  sur $\mathbf{K}[a]$ en un isomorphisme de $\mathbf{K}(X)$ vers
  $\mathbf{K}(a)$.
\end{proof}

\begin{definition}
  L'élément $a$ est dit algébrique sur $\mathbf{K}$, ou encore
  l'extension $\mathbf{K}(a)$ est dite algébrique, si et seulement si
  $I \neq \{0\}$. Il existe un polynome unitaire unique de
  $\mathbf{K}[X]$ tel que $I = (P)$ ; ce polynome $P$ est appelé
  \emph{polynome minimal} de $a$ sur $\mathbf{K}$.
\end{definition}

\begin{proposition}
  Le polynome minimal de $a$ sur $\mathbf{K}$ est irréductible sur le
  corps $\mathbf{K}$. Si $a$ n'appartient pas à $\mathbf{K}$ alors il
  est de degré au moins 2.
\end{proposition}
\begin{proof}
  \begin{itemize}
    \item Si $P$ était réductible sur $\mathbf{K}$, il existerait
      $(P_1,P_2) \in \mathbf{K}[X^2]$ tel que $P = P_1P_2$ avec $1
      \leqslant \deg(P_1)$ et $1 \leqslant \deg(P_2)$. Puisque $P(a) =
      0$, on a, comme $\mathbf{K}$ est un corps $P_1(a) = 0$ ou $P_2(a)
      = 0$ ce qui montrerait que $P_1$ ou $P_2$ appartient à $I$. Ce
      résultat serait en contradiction avec $\deg(P_1) < \deg(P)$ ou
      $\deg(P_2) < \deg(P)$.
    \item Si $P$ était de degré 1, $P = X - \alpha$ avec $P(a) = 0$ donc
      $\alpha = a$, ce qui démontre que $a$ appartiendrait à
      $\mathbf{K}$.
  \end{itemize}
\end{proof}

\begin{theoreme}
  L'élément $a$ étant algébrique, sur le corps $\mathbf{K}$, le corps
  $\mathbf{K}(a)$ est égal à l'anneau $\mathbf{K}[X]$. Tout élément de
  $\mathbf{K}(a)$ peut se mettre sous la forme $a_0 + a_1a + \cdots +
  a_{n-1}a^{n-1}$ où $n$ est le degré du polynome minimal de $a$, et
  $a_i \in \mathbf{K}$ pour $ 0 \leqslant i < n$.
\end{theoreme}

Le degré de ce polynome minimal porte le nom de degré de $a$ sur
$\mathbf{K}$.

\begin{proof}
  $P$ étant irréductible sur $K$, $I = (P)$ est maximal de donc
  $\mathbf{K}[X]/I$ est un corps. Or $\mathbf{K}[a] \simeq
  \mathbf{K}[X]/I$ donc $\mathbf{K}[a]$ est un crops inclus dans
  $\mathbf{K}(a)$ contenant $\mathbf{K}$ et $a$ , donc $\mathbf{K}[a] =
  \mathbf{K}(a)$. Soit $\Pi$ un élément du corps $\mathbf{K}[X]/I$,
  $\Pi$ est un représentant de la classe d'un polynome $A$ de
  $\mathbf{K}[X]$ ; dans $\mathbf{K}[X], \ A = QP + B,\ 0 \leqslant
  \deg(B) < \deg(P)$, ce qui montre que $\Pi$ est la classe d'un
  polynome $B$ de degré strictement inférieur à $n$. Or $B_1 \equiv B_2
  [P] \iff B_1 = B_2$. Les éléments de $\mathbf{K}[X]/I$ peuvent être
  représentés par les éléments de $\mathbf{K}_{n-1}[X]$. L'isomorphisme
  entre $\mathbf{K}[a]$ et $\mathbf{K}[X]/I$, on obtient que tout
  élément de $\mathbf{K}(a)$ s'écrit $B(a)$.
\end{proof}

\begin{corollaire}
  $\mathbf{K}(a)$ est un $\mathbf{K}$-espace vectoriel de dimension $n$
  dont une base est $(1,a,\dots,a^{n-1})$.
\end{corollaire}

\begin{remarques}
  \begin{enumerate}[label=\alph*)]
    \item Cette propriété est vraie pour $a$ algébrique. Pour $a$
      transcendant, $(a_n)_{n\in\mathbf{N}}$ est une famille libre.
    \item $P$ qui était irrédictible dans $\mathbf{K}[X]$ ne l'est pas
      dans $\mathbf{K}(a)[X]$.
  \end{enumerate}
\end{remarques}

\begin{proposition}
  Deux éléments $a$ et $b$ qui engendrent la même extension
  $\mathbf{K}(a) = \mathbf{K}(b)$ ont le même degré.
\end{proposition}

\begin{proposition}
  Si $\mathbf{K}(a)$ est une extension algébrique de $\mathbf{K}$, tout
  élément de $\mathbf{K}(a)$ est algébrique de degré inférieur ou égal
  au degré de $a$.
\end{proposition}

\section{Corps de rupture}

Il s'agit «du problème inverse» : étant donné un corps $\mathbf{K}$ et
un polynome $P$ irréductible sur $\mathbf{K}$ peut-on trouver une
extension $\mathbf{L}$ de $\mathbf{K}$ dans laquelle $P$ possède au
moins une racine.

\begin{theoreme}
  Soient $\mathbf{K}$ un corps et $P$ un polynome irréductible sur
  $\mathbf{K}[X]$. Alors il existe une extension $\mathbf{L}$ de
  $\mathbf{K}$ qui est engendrée par $\mathbf{K}$ et une racine notée $a$
  de $P$. Si de plus $\mathbf{L}_1$ est une autre extension algébrique
  simple de $\mathbf{K}$ engendrée par $\mathbf{K}$ et une racine notée
  $b$ du polynome $P$, alors $\mathbf{L}$ et $\mathbf{L}_1$ sont
  isomorphes.
\end{theoreme}
\begin{proof}
  L'idéal $I= (P)$ est un idéal maximal de $\mathbf{K}[X]$ ; l'anneau
  quotient $\mathbf{K}[X]/(P)$ est donc un corps. Il contient
  $\overline{\mathbf{K}}$ (les classes des polynomes constants)
  isomorphe à $\mathbf{K}$. On a donc construit $\mathbf{L} =
  \mathbf{K}[X]/(P)$ une extension de $\mathbf{K}$. Notons $a$ l'élément
  de $L$ qui est la classe de $X$. Tout élément de $\mathbf{L}$ est la
  classe d'un polynome de $\mathbf{K}[X]$ et même de façon plus précise
  la classe d'un polynome $A$ de degré strictement inférieur à
  $\deg(P)$. Un tel élément s'écrit $a_0 + a_1a + \cdots +
  a_{n-1}^{n-1}$ et donc $\mathbf{L} = \mathbf{K}[a] = \mathbf{K}(a)$ ce
  qui montre que $\mathbf{L}$ est une extension simple de $\mathbf{K}$.

  D'autre part, $P(a) = 0$ car la classe de $P$ est l'élément nul de
  $\mathbf{L}$ ; $\mathbf{L}$ est donc une extension algébrique simple
  de $\mathbf{K}$ contenant une racine $a$ de $P$.

  Enfin si $\mathbf{L}_1$ est une extension algébrique de $\mathbf{K}$
  engendré par $\mathbf{K}$ et une racine notée $b$ de $P$, $P$ étant
  irréductible est le polynome minimal de $b$, donc $\mathbf{L}_1 \simeq
  \mathbf{K}[X]/(P)$ donc à $\mathbf{L}$.
\end{proof}

\begin{definition}
  Un tel corps est appellé \emph{coprs de rupture} du polynome $P$.
\end{definition}

\begin{exemples}
  \begin{itemize}
    \item Construction de $\mathbf{C}$ comme corps de rupture pour le
      polynome $X^2 +1$ irréductible dans $\mathbf{R}[X]$
    \item Corps des racines d'un polynome
  \end{itemize}
\end{exemples}

\begin{theoreme}
  Tout polynome de degré supérieur ou égal à 1 admet un corps des
  racines.
\end{theoreme}
\begin{proof}
  Par récurrence sur le degré .
\end{proof}

\begin{definition}
  On appelle \emph{extension algébrique} une extension $\mathbf{L}$ d'un
  corps $\mathbf{K}$ dont tous les éléments sont algébriques sur
  $\mathbf{K}$.
\end{definition}

\begin{theoreme}[Steinitz]
  Tout corps commutatif admet un clôture algébrique $\hat{\mathbf{K}}$,
  c'est à dire une extension algébrique qui soit algébriquement close.
\end{theoreme}
\begin{proof}
  À chercher (développement ?
\end{proof}




\end{document}
