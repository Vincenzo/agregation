\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{im}}
\newcommand{\Id}{\mathop{Id}}


\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}


\title{208 Espaces vectoriels normés, applications linéaires continues.
Exemples}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section{Espaces vectoriels normés}

\begin{definition}
  Soit $E$ un espace vectoriel sur le corps $\K$. On appelle
  \emph{norme} sur $E$ tout application $N \colon E \to \R_+$ vérifant
  les axiomes :
  \begin{enumerate}[label=\textit{\roman*)}]
    \item $\forall x \in E$ \hfill $(N(x) = 0 \implies x = 0_E)$ \hfill~
    \item $\forall x \in E \ \forall \alpha \in \K$ \hfill $N(\alpha x)
      = \lvert\alpha\rvert N(x)$ \hfill~
    \item $\forall (x,y) \in E^2$ \hfill $N(x+y) \leq N(x) + N(y)$
      \hfill~
  \end{enumerate}
\end{definition}

Notons que $N(0_E) = 0$ en utilisant \textit{ii)}. On peut aussi
vérifier que \[ \forall (x,y) \in E^2, \ \lvert N(x) - N(y) \rvert
  \leq N(x - y) \] et en déduire que \[ \forall (x,y) \in E^2, \ \lvert
N(x) - N(y) \rvert \leq N(x + y). \]


\begin{exemples}
  \begin{itemize}
    \item $\K$ étant un corps commutatif, considéré comme un espace
      vectoriel sur lui-même, $x\mapsto \lvert x \rvert$ est une norme.
    \item Sur le $\K$-espace vectoriel $\K^n$ (ou le $\R$-espace
      vectoriel $\C^n$), on peut définir les normes suivantes, qui sont
      dites \emph{normes standard} sur $\K^n$. \[ \begin{matrix}
          x = (x_1, \dots, x_n) & & \nu_1(x) = \sum_{i=1}^n \lvert x_i
        \rvert ; \\ \nu_2(x) = \sum_{i=1}^n \left(x_i^2 \right)^{1/2} &
                                                                      &
      \nu_{\infty}(x) = \sup_i \lvert x_i \rvert ; \end{matrix} \] La
      vérification est aisée pour $\nu_1$ et $\nu_{\infty}$. Pour
      $\nu_2$, elle repose sur l'inégalité de Minkowski.
    \item Sur le $\K$-espace vectoriel $E$ des applications continues de
      $[0;1]$ dans $\K$, on dispose des normes  : \[ \begin{matrix}
          N_1(f) = \int_0^1 \lvert f(t) \rvert \mathrm{d}t ; & & N_2(f)
      = \left(\int_0^1 \lvert f(t) \rvert^2\mathrm{d}t \right)^{1/2} ;
  \\ N_{\infty}(f) = \sup_{t\in[0;1]} \lvert f(t) \rvert && \end{matrix}
    \]
  \end{itemize}
\end{exemples}

\begin{definition} On appelle \emph{$\K$-espace vectoriel normé (en abrégé
  e.v.n.)} tout couple $(E,N)$ où $E$ est un $\K$-espace vectoriel et
  $N$ une norme sur $E$.
\end{definition}

Lorsqu'il n'y a pas de risque de confusion, on adopte la notation $N(x)
= \lVert x \rVert$.

Si $F$ est un sous-espace vectoriel de $E$, il est clair que la
restriction $N|_F$ est une norme sur $F$.

\begin{theoremedefinition}
  Soit $(E, \lVert \cdot{} \rVert)$ un e.v.n. L'application $(x,y)
  \mapsto \lVert x - y \rVert$ est une distance sur $E$, appelée
  \emph{distance induite} par la norme. La topologie induite est dite
  \emph{topologie de la norme}.
\end{theoremedefinition}

\begin{definition}
  On appelle \emph{espace de Banach} tout e.v.n. dont l'espace métrique
  associé est complet (en abrégé, tout e.v.n. complet.)
\end{definition}

\section{Applications linéaires continues}

\begin{theoreme}
  Soient $E$ un $\K$-e.v.n., $F$ un $\K$-e.v.n., et $f$ une application
  linéaire de $E$ dans $F$. Les assertions suivantes sont équivalentes :
  \begin{enumerate}[label=\textit{(\roman*)}]
    \item $f$ est continue sur $E$ ;
    \item $f$ est continue en $0_E$ ;
    \item $f$ est bornée sur la boule unité fermée $B = \{ x \in E \mid
      \lVert x \rVert \leq 1 \}$ ;
    \item $f$ est bornée sur la sphère unité $S = \{ x \in E \mid \lVert
      x \rVert = 1 \}$ ;
    \item il existe un réel $k$ tel que : $\forall x \in E \ \lVert f(x)
      \rVert \leq k \lVert x \rVert$ ;
    \item $f$ est lipschitzienne ;
    \item $f$ est uniformémement continue.
  \end{enumerate}
\end{theoreme}

\begin{theoremedefinition}
  Soient $E$ et $F$ des $\K$-e.v.n. L'ensemble $\mathcal{L}(E,F)$ des
  applications linéaire continues de $E$ dans $F$ est un sous-espace
  vectoriel de l'espace vectoriel $L(E,F)$ des applications linéaires de
  $E$ dans $F$.

  La boule $B$ et la sphère $S$ unité définies ci-dessus, l'application
  $f \mapsto \lVert f \rVert = \sup_{x \in B}\lVert u(x) \rVert$ est une
  norme sur $\mathcal{L}(E,F)$.
\end{theoremedefinition}

\begin{theoreme}
  Soient $E$ un $\K$-e.v.n. et $F$ un espace de Banach sur un corps
  $\K$. Alors $\mathcal{L}(E,F)$ est un espace de Banach.
\end{theoreme}

\begin{theoreme}
  Soient $E$, $F$ et $G$ des $\K$-e.v.n. Pour tous $u \in
  \mathcal{L}(E,F)$ et pour tous $v \in \mathcal{L}(F,G)$, on $v \circ u
  \in \mathcal{L}(E,G)$ et $\lVert v \circ u \rVert \leq \lVert v\rVert
  \times \lVert u \rVert$.
\end{theoreme}

\begin{theoreme}
  Soient $E$ et $F$ des $\K$-e.v.n., et $u$ une surjection linéaire de
  $E$ sur $F$. Pour que $u$ soit un homéomorphisme, il faut et il suffit
  qu'il existe des réels $\alpha$ et $\beta$ strictement positifs tels
  que : \[ \forall x \in E,\ \alpha \lVert x \rVert \leq \lVert u(x)
  \rVert \leq \beta \lVert x \rVert .\]
\end{theoreme}

\begin{definition}
  Soient $E$ et $F$ des $\K$-e.v.n. On appelle \emph{isomorphisme de
  l'e.v.n. $E$ sur l'e.v.n. $F$} tout isomorphisme de l'espace vectoriel
  $E$ sur l'espace vectoriel $F$ qui est un homéomorphisme.
\end{definition}


\section{Quelques exemples}

\begin{theoreme}
  Soit $E$ un $\K$-e.v.n. L'application $(x,y) \mapsto x+y$ de $E^2$
  dans $E$ est uniformémement continue. L'application $(\alpha,x)
  \mapsto \alpha x$ de $\K\times E$ dans $E$ est continue.
\end{theoreme}

\begin{theoreme}
  Soit $E$ un $\K$-e.v.n. Pour tout $a \in E$, la translation $\tau_a
  \colon x \mapsto x + a$ est une isométrie ; pour tout $\alpha \in \K
  \setminus \{0\}$, l'homothétie $h_{\alpha} \colon x \mapsto \alpha x$
  est un isomorphisme d'e.v.n.
\end{theoreme}

\begin{theoreme}
  Soient $X$ un espace topologique, $E,F,G$ des $\K$-e.v.n. Soient $A$
  une partie de $X$ et $a$ un point de $\overline{A}$.

  \begin{enumerate}[label=\textit{\roman*)}]
    \item Si $f$ et $g$ sont des fonctions de $X$ vers $E$, définies sur
      $A$, telles que $\lim_{x\to a, x\in A} f(x) = l$ et $\lim_{x\to a,
      x\in A} g(x) = m$, alors : \[ \lim_{x\to a, x\in A} [ f(x) + g(x)]
      = l + m \]
    \item Si $f$ est une fonction de $X$ vers $E$, $\varphi$ est une
      fonction de $X$ vers $\K$ définies sur $A$, telles que $\lim_{x\to
        a, x\in A} f(x) = l$ et $\lim_{x\to a, x\in A} \varphi(x) =
        \alpha $, alors : \[ \lim_{x\to a, x\in A} [ \varphi(x) f(x) ] =
        \alpha l \]
  \end{enumerate}
\end{theoreme}

On peut insister sur les isométries suivantes.

\begin{proposition}
  Soient $E,F,G$ des $\K$-e.v.n.

  \begin{enumerate}[label=\textit{\roman*)}]
    \item Pour toute application bilinéaire et continue $u \in
      \mathcal{L}_2(E,F,G)$ et tout $x \in E$, l'application $u_x \colon
      y \mapsto u(x,y)$ de $F$ dans $G$ est linéaire et continue.
    \item L'application $\tilde{u} \colon E \to \mathcal{L}(F,G), x
      \mapsto u_x$ est continue.
    \item L'application $\psi \colon \mathcal{L}_2(E,F,G) \to
      \mathcal{L}[E,\mathcal{L}(F,G)], u \mapsto \tilde{u}$ est un
      isomorphisme algébrique qui conserve la norme ; on le qualifie
      d'isométrie canonique ; $\psi^{-1}$ est déterminé par : \[ \forall
        v \in \mathcal{L}[E,\mathcal{L}(F,G)] \psi^{-1}(v) = (x,y)
      \mapsto (v(x)(y) \]
  \end{enumerate}
\end{proposition}


\end{document}
