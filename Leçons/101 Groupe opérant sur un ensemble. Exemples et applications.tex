\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{enumitem}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}

\parindent0pt

\usepackage{babel}

\title{101 -- Groupe opérant sur un ensemble. Exemple et applications.}
\author{Vincent-Xavier \bsc{Jumel}}
\date{17/09/2016}

\begin{document}
\maketitle

$E$ désigne ici un ensemble non vide.

\section{Définition et exemples}

\begin{definition}
  Soit $G$ un groupe de neutre $e$ et $E$ un ensemble. On dit que $G$
  opère sur $E$ si et seulement si la loi de composition externe $(a,x)
  \mapsto ax$ vérifie les axiomes suivants :
  \begin{itemize}
    \item $\forall (a,b) \in G^2,\ \forall x \in E,\ a(bx) = (ab)x$
    \item $\forall x \in E,\ ex = x$
  \end{itemize}
\end{definition}

\begin{theoreme}
  Soit $f_a$ l'application $x \mapsto ax$ de $E$ dans lui-même.
  L'application $a \mapsto f_a$ est un morphisme du groupe $G$ dans
  $\mathfrak{S}_E$ son groupe symétrique. Inversement si $\varphi : G
  \to \mathfrak{S}_E$ est un morphisme de groupes, en posant $ax =
  \phi(a)(x)$ pour $a\in G$ et $x\in E$, on fait opèrer $G$ sur $E$.
  \begin{proof}
    On peut réécrire les axiomes sous la forme :
    \begin{itemize}
      \item $f_a \circ f_b = f_{ab}$
      \item $f_e = Id$
    \end{itemize}
    D'après $f_a \circ f_{a^{-1}} = f_e$, les $f_a$ sont des bijections
    ; $a \mapsto f_a$ est un application de $G$ dans $\mathfrak{S}_G$
  \end{proof}
\end{theoreme}

\begin{definition}Si le morphisme $a \mapsto f_a$ est injectif, on dit
  que $G$ opère fidèlement sur $E$.
\end{definition}

\begin{remarque}
  On peut :
  \begin{itemize}
    \item faire opèrer sur $E$ tout sous-groupe $F$ de $G$ en utilisant
      la restriction à $F\times E$ de la loi $(a,x) \mapsto ax$ ;
    \item faire opèrer $G$ sur toute partie $X$ non vide de $E$ et
      invariante (au sens $f_a(X) = X$ pour tout $a\in G$.)
  \end{itemize}
\end{remarque}

\begin{exemples}
  \begin{enumerate}[label=\alph*)]
    \item Soit $E$ un ensemble. En considérant l'isomorphisme identique
      de $\mathfrak{S}_E$ sur lui même, on constate que $\mathfrak{S}_E$
      opère fidèlement sur $E$ par $(s,x) \mapsto s(x)$
    \item On peut faire opèrer un groupe sur lui-même :
      \begin{itemize}
        \item par $(a,x) \mapsto \tau_a(x) = ax$. On dit alors que $G$
          opère par \emph{translation à gauche} sur $E$.
        \item par $(a,x) \mapsto axa^{-1}$. On dit alors que $G$ opère
          par \emph{conjugaison} (ou par \emph{automorphisme
          intérieurs})
      \end{itemize}
    \item on peut signaler les exemples géométriques. $E$ étant un
      espace euclidien, on peut faire opèrer sur $E$ tous les groupes de
      transformation classique.
  \end{enumerate}
\end{exemples}

\begin{theoreme}
  Soit $G$ un groupe opérant sur l'ensemble $E$. On fait opèrer $G$ sur
  $\mathscr{P}(E)$ en posant, pour $a\in G$ et $X\in \mathscr{P}(E)$
  \[ a X = \{ z \in E | \exists x \in X,\ z = ax \} \]
\end{theoreme}

\section{Trajectoire (ou orbite) d'un élement}

\begin{theoreme} Soit $G$ un groupe opérant sur un ensemble $E$. La
  relation : \[ \text{«} \exists a \in G,\  y = ax \text{»} \] est une relation
  d'équivalence sur $E$.
\end{theoreme}
\begin{definition}
  La classe d'équivalence de la relation définie ci-dessus s'appelle
  \emph{orbite} (ou \emph{trajectoire}) de $x \in E$ suivant $G$. On la
  note $O(x)$ ou $G \cdot x$.
\end{definition}

\begin{exemples}
  \begin{enumerate}[label=\alph*)]
    \item Les exemples géométriques illustrent la notion de trajectoire.
    \item Si $G$ opère sur lui-même par translation à gauche, toute
      orbite est $G$ lui-même.
    \item Soit $s\in \mathfrak{S}(E)$, ($E$ fini) ; l'orbite de $x\in E$
      suivant $s$ est l'orbite de $x$ suivant le groupe cyclique
      engendré par $s$.
  \end{enumerate}
\end{exemples}

\begin{definition}
  On dit que $G$ opère transitivement sur $E$ si et seulement si la
  seule orbite est $E$, ce qui se traduit par \[ \forall (x,y) \in E^2,
    \  \exists a\in G,\  y = ax \]
\end{definition}

\begin{exemples}
  \begin{enumerate}[label=\alph*)]
     \item $G$ opère sur lui-même par translation à gauche, fidèlement
       et transitivement.
     \item $\mathfrak{S}(E)$ opère transitivement sur $E$ (ici $ y = a
       x$, où $a$ est la transposition qui échange $x$ et $y$).
     \item Si $s\in \mathfrak{S}(E)$, ($E$ fini), le groupe cyclique
       engendré par $s$ opère transitivement si et seulement si $s$ est
       un cycle de longueur $n = \#E$ ($s$ est alors appelé
       permutation circulaire).
     \item $G$ n'opère transitivement sur $G$ par conjugaison que si $G
       = \{e\}$ ; en effet la trajectoire de $e$ est $\{e\}$.
     \item $G$ opère transitivement par translation à gauche sur
       l'ensemble $Q$ des classes à gauche suivant un sous-groupe $H$
       ( ici $G$ opère par $(a,xH) \mapsto axH$, et l'orbite de $H \in
       Q$ est $H$)
     \item $G$ opère (par loi induite) sur toute trajectoire (partie de
       $E$ non vide et invariante) et il est clair que c'est
       transitivement.
   \end{enumerate}
\end{exemples}

\begin{theoreme}
  $G$ opère transitivement sur chaque trajectoire.
\end{theoreme}

\section{Stabilisateur ; applications}

\begin{definition}
  Soit $G$ opérant sur $E$ et $X \subset E$. Les ensembles \[ G_X = \{ a
      \in G | a \cdot X = X \} \text{ et } F_X = \{ a \in G | a \cdot x =
  x \forall x \in X \} \] s'appellent stabilisateur de $X$ et fixateur
  de $X$.
\end{definition}

\begin{theoreme}
  \begin{itemize}
    \item $G_X$ et $F_X$ sont des sous-groupes de $G$ ;
    \item $F_X \vartriangleleft G_X$ ;
    \item Si $X = \{x\}$ alors $F_X = G_X$ qui est noté $G_x = \{ a \in
      G | a x = x \}$
  \end{itemize}
\end{theoreme}
\begin{proof}
  $e \in G_X$ et pour pour tous $a\in G_X$ et $b\in G_X$ on a :
  \[ (ab)\cdot X = a \cdot (b \cdot X) = a \cdot X = X \ ; \ X = (a^{-1}
  a )\cdot X = a^{-1} \cdot X \]
  ce qui montre que $G_X$ est un sous-groupe de $G$.

  Le groupe $G_X$ opère sur $X$ (invariant pour toute $x\mapsto a\cdot
  x, a \in G_X$), et $F_X$ est le noyau du morphisme associé, d'où $F_X
  \vartriangleleft G_X$.
\end{proof}

\begin{exemple}
  Dans un plan affine euclidien sur le quel opère le groupe des
  isométries, si $ABCD$ est un rectangle (mais pas un carré) le
  stabilisateur de $\{ A, B, C, D\}$ est $\{a^2, a, b, ab \}$ où $a$ et
  $b$ sont les symétries orthogonales par rapport aux médiatrices de
  $AB$ et $AD$. % et est donc isomorphe au groupe de Klein
\end{exemple}

\textsc{Cas particulier} -- Soit $G$ opérant sur $G$ par conjugaison et
$X\in G$. Les groupes \[ G_X = \{a\in G \mid aXa^{-1} = X \} \text{ et }
F_X = \{a\in G \mid axa^{-1} = x \forall x \in X \} \] sont appelés
\emph{normalisateur} et \emph{centralisateur} de X.

En particulier, si $H$ est un sous-groupe de $G$, il est clair que $H
\vartriangleleft G_H$ et que si $K$ est un sous-groupe de $G$ tel que $H
\vartriangleleft K$ alors $K \subset G_H$ ; $G_H$ est donc le plus grand
sous-groupe de $G$ dans lequel $H$ est distingué. Il en résulte que $H
\vartriangleleft G$ équivaut ) $G_H = G$.

\begin{theoreme}
  Soit $G$ opérant sur $E$, $(a,x,y) \in G \times E \times E$ tels que
  $y = a \cdot x$. Alors $G_y = aG_xa^{-1}$ (les sous-groupes $G_x$ et
  $G_y$ sont conjugués).
  \begin{proof}
    Soit $s\in G$ ; $(asa^{-1}) \cdot y = a\cdot (s \cdot x) = a\cdot x
    = y$ fournit $asa^{-1} \in G_y$. D'où $aG_xa^{-1} \subset G_y$. De
    même, $a^{-1}G_ya \subset G_x$
  \end{proof}
\end{theoreme}

\begin{theoreme} Soit $G$ opérant sur $E$ et $x\in E$. La trajectoire de
  $x$ est finie si et seulement si $G_x$ est d'indice fini dans $G$ et
  on a \[ (G:G_x) = \#G\cdot x \]
  \begin{proof} On considére pour cela $f : G \to E$ définie par $f(a) =
    a\cdot x$ et on utilise la décomposition canonique d'une
    application : il existe une bijection de $f(G) = G \cdot x$ sur $G /
    \mathscr{R}$, $a\mathscr{R}b$ signifiant $a\cdot x = b\cdot x$, cad
    $(b^{-1} a) \cdot x = (b^{-1} b) \cdot x = x$, cad $b^{-1}a \in G_x$;
    $\mathscr{R}$ est donc l'équivalence à gauche dans $G$, suivant
    $G_x$.
  \end{proof}
\end{theoreme}

\begin{exemple}
  Si $G$ opère sur lui-même par conjugaison et si $H$ est un sous-groupe
  distingué de $G$, le nombre de sous-groupes conjugués de $H$ est égal
  à l'indice de $(G : G_H)$ de son stabilisateur.
\end{exemple}

\textsc{Application : Équation aux classes} -- Supposons que $G$ opère
sur un ensemble fini $E$ de cardinal $n$. Choisissons un $x$, dans
chaque orbite suivant $G$, ($1 \leqslant i \leqslant q$). Les orbites
$(G \cdot x_i)_{1 \leqslant i \leqslant q}$ forment ainsi une partition
de $E$. D'où : \[ n = \#E = \sum_{i=1}^q G \cdot x_i = \sum_{i=1}^q (G :
G_{x_i}) \] Si $G$ est un groupe fini qui opère sur lui-même par
conjugaison, les élements du centre $Z$ de $G$ sont caractérisé par \[
  \# G\cdot x = (G : G_x) = 1, \] et on obtient \[ \#G = \#Z +
\sum_{i\in I} (G : G_{x_i}) \] cette sommation étant étendu à tous les
éléments tels que $(G : G_{x_i}) > 1$. En particulier, si $\# G = p^n$
où $p$ est premier et $n > 0$, $(G : G_{x_i}) = \dfrac{\# G}{\# G_{x_i}}
= p^{\alpha}$ avec $\alpha >0$ pour $i\in I$. On en déduit que $\# Z$
est divisible par $p$ (en particulier $Z \neq \{e\}$).

On en déduit que tout groupe $G$ de cardinal $p^2$, $p$ premier est
abélien (sinon on aurait $\# Z = p$ ; il existerait $x\in G\backslash Z$
; on aurait $x\in G_x\backslash Z$, $\#G_x > p$, $G_x = G$ et $x\in Z$ :
contradiction).

\textsc{Application : Théorème de Sylow} -- Voir développement sur le sujet.

\textsc{Application : polynomes symétriques} -- on les définit comme les
polynomes invariants par action d'un sous-groupe de $\mathfrak{S}$.

%\cite*{RDO1}
%nocite{*} pour tout faire apparaître. L'étoile après la commande inhibe
%l'affichage.

\begin{thebibliography}{9}
  \bibitem{RDO1}
  Edmond Ramis, Claude Deschamps et Jacques Odoux,
  \emph{Cours de mathématiques 1. algèbre},
  Dunod,
  2\up{ème} édition,
  2001,
\end{thebibliography}


\end{document}
