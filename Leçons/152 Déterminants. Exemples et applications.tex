\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}


\parindent0pt

\usepackage{babel}

\title{152 Déterminant. Exemples et applications}
\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

Dans cette leçon, $\K$ est un corps, et $E$, $F$ (avec d'éventuels
indices) désignent des espaces vectoriels sur $\K$.

\section{Généralités}

\begin{definition}
  On dit qu'une application $f : E_1 \times \cdots \times E_p \to F$ est
  $p$-linéaire si, et seulement si, les applications partielles sont
  linéaires.
\end{definition}

\begin{definition}
  Une application $p$-linéaire de $E_1 \times \cdots \times E_p$ dans
  $\K$ est dite une forme $p$-linéaire.
\end{definition}

\begin{proposition}
  \begin{itemize}
    \item Toute application linéaire de $E$ dans $F$ est 1-linéaire ;
    \item l'image d'un $p$-uple dont un vecteur est nul par une
      application $p$-linéaire est nulle ;
    \item l'application nulle de $E_1 \times \cdots \times E_p$ dans $F$
      est à la fois linéaire et $p$-linéaire ;
    \item soit pour $p \geqslant 2$ une application $p$-linéaire non
      nulle $f : E_1 \times \cdots \times E_p \to F$, alors $f$ n'est
      pas linéaire.
  \end{itemize}
\end{proposition}

\begin{theoreme}
  L'ensemble des applications $p$-linéaires forme un $\K$-espace
  vectoriel, sous espace vectoriel de l'ensemble des applications de
  $E_1 \times \cdots \times E_p$ dans $F$.

  On le note $\mathcal{L}_p(E_1, \dots, E_p, F)$.
\end{theoreme}

On se limite par la suite au cas où $E_1 = \dots = E_p = E$ et on notera
$\mathcal{L}_p(E,F)$ l'espace des applications $p$-linéaires de $E^p$
dans $F$ et même $\mathcal{L}_p(E)$ l'ensemble des formes $p$-linéaires.

\begin{definition}
  On dit qu'une forme $p$-linéaire $f$ sur $E$ est :
  \begin{itemize}
    \item symétrique si et seulement si pour tout $\sigma \in
      \mathfrak{S}_p,\ \sigma(f) = f$ ;
    \item antisymétrique si et seulement si pour tout $\sigma \in
      \mathfrak{S}_p,\ \sigma(f) = \varepsilon(\sigma)f$
    \item alternée si et seulement si l'image de tout $p$-uple de $E^p$
      contenant deux vecteurs égaux est nulle.
  \end{itemize}
\end{definition}

\begin{theoreme}
  Soit $f \in \mathcal{L}(E)$. Pour que $f$ soit symétrique (resp.
  antisymétrique) il faut et il suffit que, pour toute transposition
  $\tau \in \mathfrak{S}_p,\ \tau(f) = f$ (resp. $\tau(f) = -f$.)
\end{theoreme}

\begin{theoreme}
  Soit $f \in \mathcal{L}(E)$. Si $f$ est alternée, alors $f$ est
  antisymétrique.
\end{theoreme}

\begin{theoreme}
  Soit $f \in \mathcal{L}(E)$. Si le corps $\K$ n'est pas de
  caractéristique 2 et si $f$ est antisymétrique, alors $f$ est
  alternée.
\end{theoreme}

\begin{theoreme}
  L'ensemble des formes $p$-linéaires sur $E$ symétrique (resp.
  alternée) forme un espace vectoriel, noté $\mathcal{S}_p(E)$ (resp.
  $\mathcal{A}_p(E)$.)
\end{theoreme}

\begin{theoreme}
  Soit $f \in \mathcal{A}_p(E)$. On ne change pas la valeur prise par
  $f$ sur un $p$-uple de $E^p$ si on ajoute à l'un des vecteurs une
  combinaison linéaire des autres. En particulier, $f$ prend la valeur 0
  sur tout $p$-uple formant une famille liée.
\end{theoreme}

\section{Déterminant}

Puisqu'on travaille avec une forme $p$-linéaire, donnons nous une base
de l'esapce duale $E^*$ : $(e_1^*, \dots, e_n^*)$.

L'application de $E^n$ dans $\K$ définie par $(x_1, \dots, x_n) \mapsto
\prod_{j=1}^n\left\langle e_j^*, x_j\right\rangle$ est une forme
$n$-linéaire.

On peut rendre cette forme antisymétrique en sommant cette forme sur
l'ensemble des permutatioins, à la signature près \footnote{à détailler}


\begin{definition}
  On appelle \emph{déterminant} la forme $n$-linéaire définie sur la
  base $e$, notée $\det_e$ définie par \[ \det_e (x_1, \dots, x_n)
  \mapsto \sum_{\sigma\in \mathfrak{S}_n} \varepsilon(\sigma)
  \prod_{j=1}^n\left\langle e_j^*, x_{\sigma(j)} \right \rangle \]
\end{definition}

\begin{proposition}
  En particulier, on a $\det_e (e_1, \dots, e_n) = 1$

  De plus, $\mathcal{A}_n(E)$ est un espace vectoriel non réduit à
  $\{0\}$ puisqu'il contient $\K\det_e$ comme sous espace vectoriel.
\end{proposition}

\begin{theoreme}
  Soit $E$ un $\K$-espace vectoriel admettant une base finie $e =
  (e_1,\dots, e_n)$. Alors :
  \begin{enumerate}[label=(\roman*)]
    \item Pour tout entier $p >n$, la seule forme $p$-linéaire alternée
      sur $E$ est l'application nulle.
    \item Il existe une et une seule forme $n$-linéaire alternée
      prenant la valeur 1 sur $(e_1,\cdots,e_n)$.
  \end{enumerate}
\end{theoreme}

\begin{definition}
  On appelle cette forme $n$-linéaire \emph{déterminant dans la base
  $e$}
\end{definition}

\begin{proposition}
  $\mathcal{A}_n(E)$ est un espace vectoriel de dimension 1 admettant
  $\det_e$ comme base.
\end{proposition}

\begin{definition}
  On parle du scalaire $\det_e(x_1,\dots,x_n)$ comme \emph{déterminant
  d'une famille de vecteurs}. Il admet les expressions \[
    \sum_{\sigma\in \mathfrak{S}_n} \varepsilon(\sigma)
    \prod_{j=1}^n\left\langle e_j^*, x_{\sigma(j)} \right \rangle \] ou
  \[ \sum_{\sigma\in \mathfrak{S}_n} \varepsilon(\sigma)
  \prod_{j=1}^n\left\langle e_{\sigma(j)} x_j \right\rangle.\]
\end{definition}

\begin{proposition}
  Soient $E$ un $\K$-espace vectoriel de dimension $n$ non nulle, et $a
  = (a_1,\dots,a_n)$ une base de $E$. Alors le système $a$ admet un
  déterminant non nul dans toute base de $E$.
\end{proposition}

\begin{proposition}
  Soit $E$ un $\K$-espace vectoriel de dimension non nulle $n$. Tout
  système lié de $n$ vecteurs de $E$ admet un déterminant nul dans toute
  vase de $E$.
\end{proposition}

\begin{proposition}
  Soit $u$ un endomorphisme d'une $\K$-espace vectoriel de dimension
  finie non-nulle $n$. Il existe un et un seul scalaire $\lambda$ tel
  que pour toute forme $f \in \mathcal{A}_n(E)$ et pour tout $n$-uple
  $(x_1,\dots,x_n)$ de $E^n$, on ait : \[ f(u(x_1),\dots,u(x_n)) =
  \lambda f(x_1,\dots,x_n) \]
\end{proposition}

\begin{definition}
  Le scalaire ainsi défini s'appelle \emph{déterminant de $u$} et se
  note $\det_e(u)$.
\end{definition}

\begin{proposition}
  Soit $E$ un $\K$-espace vectoriel de dimentsion finie non nulle $n$.
  Alors :
  \begin{enumerate}[label=\alph*)]
    \item Le déterminant de l'application identique de $E$ est 1
    \item $\forall \lambda \in \K,\ \forall u \in \mathcal{L}(E) \
      \det(\lambda u) = \lambda^n\det(u)$
    \item $\forall u \in \mathcal{L}(E) \ \det \vphantom{e}^tu = \det u$
    \item L'application $\det$ définit un morphisme de groupe.
  \end{enumerate}
\end{proposition}

\begin{proposition}
  Soient $E$ un $\K$-espace vectoriel de dimension finie non-nulle $n$
  et $u$ un endomorphisme de $E$. $u$ est inversible si et seulement si
  son déterminant n'est pas nul et alors \[ \det u^{-1} = (\det u)^{-1}
  \]
\end{proposition}

Les propositions se déclinent aux matrices carrés en utilisant la
correspondance entre endomorphisme de $E$ dans une base et éléments de
$\mathcal{M}_n(\K)$.

Cependant, le calcul du déterminant est facilité par la représentation
matricielle. En particulier $\det(x_{\sigma(i)}, \dots, x_{\sigma(n)}) =
\varepsilon(\sigma)\det(x_1,\dots,x_n)$ où $\sigma \in \mathfrak{S}_n$.
En particulier, si $\sigma$ est une transposition (échange de deux
colonnes), on multiplie le résultat par -1.

On a aussi la caractérisation suivante en utilisant le fait que la
$j$-ième application partielle d'une forme $n$-linéaire est linéaire.

\begin{lemme}
  Soit $M$ une $\K$-matrice carrée d'ordre $n = p+q$ de la forme \[ M =
  \left[\begin{matrix} A & C \\ 0 & B \end{matrix}\right] \] avec $A \in
    \mathcal{M}_p(\K)$, $B\in \mathcal{M}_q(\K)$ et $C \in
    \mathcal{M}_{p,q}(\K)$ et 0 la matrice nulle (ici de
    $\mathcal{M}_{q,p}(\K)$.)

    Alors $\det M = (\det A)(\det B)$
\end{lemme}

\begin{theoreme}
  Ce lemme s'étend par récurrence à toute matrice triangulaire
  supérieures par blocs (éventuellement réduit à des matrices de
  dimension 1) et même triangulaire inférieures par transposition.
\end{theoreme}

\begin{proposition}
  Le déterminant d'une matrice diagonale est le produit des éléments
  diagonaux.
\end{proposition}

\begin{proposition}
  Développement d'un déterminant suivant une rangée (ligne ou colonne)
\end{proposition}

\begin{definition}
  Soit $M = [ a_{ij} ]$ une $\K$-matrice carrée d'ordre $n > 1$. On
  appelle :
  \begin{itemize}
    \item mineur relatif à l'élément $a_{ij}$ le déterminant de la
      matrice carrée d'ordre $n-1$, $M_{ij}$ déduite de $M$ en
      supprimant la $i$-ième ligne et la $j$-ième colonne ;
    \item cofacteur de $a_{ij}$ le scalaire $(-1)^{i+j} \det M_{ij}$.
  \end{itemize}
\end{definition}

\begin{proposition}
  Soit $M = [ a_{ij} ]$ une $\K$-matrice carrée d'ordre $n > 1$ ; avec
  les notations précédentes, on a l'égalité, pour tout $1 \leqslant j
  \leqslant n$ \[ \det M = \sum_{k=1}^n a_{kj}A_{kj} \] où $A_{kj}$ est
  le cofacteur défini ci-dessus.
\end{proposition}

\begin{definition}
  L'expression ci-dessus s'appelle \emph{développement suivant une
  colonne.}
\end{definition}

\textsc{Application : déterminant de Vandermonde}

À l'exclusion des propositions faisant intervenir la notion d'éléments
inversibles, on peut étendre la théorie aux déterminants sur un
$\mathcal{A}$-module et donc considérer qu'on prend calcule des
déterminants sur un anneau (ou éventuellement dans son corps de
fractions rationnelles.

\textsc{Application : Théorème de \bsc{Cayley-Hamilton}}

\textsc{Application : formules de Cramer}

\textsc{Résultant de deux polynomes}

\end{document}
