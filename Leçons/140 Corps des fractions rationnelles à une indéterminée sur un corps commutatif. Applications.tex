\documentclass[a4paper,12pt,twocolumn,landscape,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage[upright]{kpfonts}
\usepackage[bookmarks=false,colorlinks,linkcolor=blue,pdfusetitle]{hyperref}
\usepackage[a4paper,vmargin=1.0cm,hmargin=2cm,includefoot]{geometry}
\usepackage{lastpage}
\usepackage{fancyhdr}
\usepackage{multicol}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{xfrac}
\usepackage[np,autolanguage]{numprint}
\usepackage{tikz}
\usepackage{tkz-euclide}
\usepgflibrary{shapes}
\usetikzlibrary{matrix,arrows,decorations.pathmorphing,circuits.ee.IEC}
\usepackage{tabularx}
\usepackage{tabvar}
\usepackage{amsthm}
\usepackage{xstring}
\usepackage{babel}

\frenchbsetup{og=«,fg=»}
\pdfminorversion 7
\pdfobjcompresslevel 3
\count1=\year \count2=\year
\ifnum\month<8\advance\count1by-1\else\advance\count2by1\fi
\pagestyle{fancy}
\cfoot{\textsl{\footnotesize{Année \number\count1/\number\count2}}}
%\lfoot{\textsl{\footnotesize{LAL 1.3 Vincent-Xavier \textsc{Jumel}}}}
\rfoot{\textsl{\footnotesize{Page \thepage/ \pageref{LastPage}}}}
\lhead{}
\rhead{}
\renewcommand{\headrulewidth}{0pt}
\renewcommand{\footrulewidth}{0pt}
\newcommand{\e}{\varepsilon}
\newcommand{\ens}{\mathbb}
\newcommand{\R}{\ens{R}}
\newcommand{\C}{\ens{C}}
\newcommand{\N}{\ens{N}}
\newcommand{\Z}{\ens{Z}}
\newcommand{\Pc}{\ens{P}}
\newcommand{\K}{\ens{K}}
\newcommand{\U}{\ens{U}}
\newcommand{\ssi}{si, et seulement si }
\renewcommand{\implies}{\DOTSB \;\Rightarrow \;}
\renewcommand{\iff}{\DOTSB \;\Leftrightarrow \;}
\everymath{\displaystyle\everymath{}}
\newcommand{\Ccal}{\mathcal{C}}
\parindent0pt
\columnsep25pt

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}



\title{\StrDel{\scantokens\expandafter{\jobname\noexpand}}{"}}
\author{Vincent-Xavier Jumel}
\date{30 avril 2014}

\makeatletter
\hypersetup{
  pdftitle={\scantokens\expandafter{\jobname\noexpand}},
  %pdfsubject={Modèle de document LaTeX},
  %pdfkeywords={LaTeX, modèle},
  pdfauthor={Vincent-Xavier Jumel}
}

\renewcommand{\maketitle}%
{\begin{center}%
\framebox{%
    \begin{minipage}{1.0\linewidth}%
      \begin{center}%
        \large \@title \\%
         \@author -- \@date%
      \end{center}%
    \end{minipage}}%
  \normalsize%
  %\vspace{1cm}%
\end{center}
}
\makeatother

\renewcommand{\K}{\ens{K}}
\newcommand{\KX}{\K[X]}
\newcommand{\Kx}{\K(X)}
\renewcommand{\deg}{\mathop{d\degres}}

\begin{document}
\maketitle

\section{Généralités}

On considère $\K$ un corps et $\KX$ son anneau des polynomes
d'indéterminée $X$.

\begin{proposition}
  La relation définie sur $\KX \times \KX^*$ par $aq - bp = 0$ est une
  relation d'équivalence.
\end{proposition}

\begin{theoreme}
  L'anneau $\KX \times \KX^*$ quotienté par la relation précédente est un
  corps.
\end{theoreme}

\begin{definition}
  Le corps précédent est appellé corps des fractions rationnelles sur
  l'anneau $\KX$, noté $\Kx$ dont les éléments sont noté $\dfrac{P}{Q}$
\end{definition}

\begin{proposition}
  Soient $P,Q \in \KX^*$.

  Si $P \wedge Q = 1$, alors $\dfrac{P}{Q}$ est irréductible et
  l'écriture est unique.
\end{proposition}

\begin{proposition}
  Soit $F \in \Kx,\  F\neq 0 \, F = \dfrac{N}{D}$ un représentant.

  Alors $F^{-1}$ existe et un représentant s'écrit $F^{-1} =
  \dfrac{D}{N}$
\end{proposition}

\begin{proposition}
  $\varphi:\KX\to\Kx, P\mapsto \dfrac{P}{1}$ est un morphisme injectif
  de corps.
\end{proposition}

\begin{corollaire}
  On peut écrire, à isomorphisme près, que $\KX \subset \Kx$.
\end{corollaire}

\begin{proposition}
  Soit $F = \sfrac{P}{Q} \in \Kx^*$.

  L'entier $r = \deg{P} - \deg{Q}$ ne dépend pas du représentant choisi.
\end{proposition}

\begin{definition}
  L'entier $r$ défini ci-dessus est le degré de $F$.

  $\deg{0} = - \infty$
\end{definition}

\begin{proposition}
  Soient $F,G \in \Kx$.

  \begin{enumerate}
    \item[(i)] $\deg{F+G} \leq \max{\deg{F},\deg{G}}$
    \item[(ii)] $\deg{FG} = \deg{F} + \deg{G}$
  \end{enumerate}
\end{proposition}

\begin{corollaire}
  $\Kx$ n'est pas algébriquement clos.
\end{corollaire}

\begin{definition}
  Soit $F = \sfrac{P}{Q} \in \Kx$.

  On définit $F'$ sa dérivée formelle, par $F' = \dfrac{P'Q - Q'P}{Q^2}$
  où $P'$ et $Q'$ sont les polynomes dérivés formels de $P$ et $Q$.
\end{definition}

\begin{proposition}
  Cette définition est cohérente avec la dérivation formelle des
  polynômes.
\end{proposition}

\begin{definition}
  Soit $F \in \Kx$, d'écriture irréductible $\dfrac{P}{Q}$.

  Alors les racines de $Q$ sont appellées les pôles de $F$.
\end{definition}

\begin{definition}
  Soit $F$ une fonction rationnelles. Notons $D_{\K}(F)$ l'ensemble $\K$
  privé des pôles de $F$.

  L'application $\widetilde{F} : \D_{\K}(F) \to \K,\ x \mapsto
  \widetilde{F}(x) = \dfrac{\widetilde{P}(x)}{\widetilde{Q}(x)}$ est
  appellée fonction rationnelle.
\end{definition}

\begin{theoreme}
  Si $\K$ est un sous-corps de $\C$, et $\forall x \in D_{\K}(F) \cap
  D_{\K}(G),\ \widetilde{F}(x) = \widetilde{G}(x)$, alors $F = G$.
\end{theoreme}

\section{Décomposition en élément simple}

\begin{theoreme}[Décomposition en éléments simples]
  Soit $F\in \Kx,\ F\neq 0$, de forme réduite $\sfrac{N}{D}$. Soit $D =
  D^{\alpha_1} \cdots D^{\alpha_n}$ la décomposition de $D$ en facteurs
  irréductibles de $\KX$. On peut écrire, de manière unique : \[ F = E +
    \sum_{i=1}^n \left( \sum_{k=1}^{\alpha_i} \dfrac{A_{i,k}}{D_i^k}
  \right) \]
\end{theoreme}

$E$ est la partie entière de $F$ et s'obtient par division euclidienne
de $N$ par $D$.

Dans un corps algébriquement clos, comme $\C$, $\dfrac{A_{i,k}}{D_i^k}$
sont les éléments simples du pôle $D_i$ avec la multiplicité $k$.

En pratique dans $\C[X]$, on peut utiliser le fait que $\C$ est
algébriquement clos et que les seuls polynomes irréductibles sont de la
forme $\prod_{i=1}^n (X-a_i)^{\alpha_i}$

\begin{proposition}
  Soit $a$ un pôle simple de $F \in \C(X)$, $F\neq 0$ de forme
  irréductible $\sfrac{N}{D}$ . On a \[ F =
  \frac{\widetilde{N}(a)}{\widetilde{D'}(a)(X-a)} + G \] avec
  $\widetilde{G}(a) \neq 0$.
\end{proposition}

\begin{remarque}
  Si on connait une factorisation de $D = D_1(X-a)$, on peut écrire $F =
  \dfrac{\widetilde{N}(a)}{\widetilde{D_1}(a)(X-a)} + G$ avec
  $\widetilde{G}(a) \neq 0$.
\end{remarque}

\begin{proposition}
  Soit $a$ un pôle de multiplicité $h>1$ de $F \in \C(X)$, $F\neq 0$ de
  forme irréductible $\sfrac{N}{D}$.

  On peut écrire $D = (X-a)^h D_0$, avec $D_0 \in \C[X]$ et
  $\widetilde{D_0}(a) \neq 0$. Posons $D_1(T) = D_0(T + a),\ N_1(T) =
  N(T + a),\ F_1(T) = F(T + a)$. On a $F_1(T) =
  \dfrac{N_1(T)}{T^hD_1(T)}$.

  Si $N_1 = (a_h + \cdots + a_1T^{h-1})D_1 + T^hS$ est la division de
  $N_1$ par $D_1$ selon les puissances croissantes, à l'ordre $h-1$,
  alors \[ F_1(T) = \frac{a_1}{T} + \cdots + \frac{a_h}{T^h} +
  \frac{S(T)}{D_1(T)} \] et donc \[ F = \frac{a_1}{X-a} + \cdots +
  \frac{a_h}{(X-a)^h} + \frac{S(X+a)}{D_0} \]
\end{proposition}

Dans $\R(X)$, il faut distinguer deux types d'éléments simples, qui
correspondent aux facteurs irréductibles dans $\R[X]$ :

\begin{definition}
  Les éléments simples de la forme $\dfrac{\alpha}{(X-a)^n},\ a,\alpha
  \in \R$ sont appellés éléments simples de première espèce.

  Les éléments simples de la forme $\dfrac{\alpha X + \beta}{(X^2 + pX +
  q)^n},\ a,\alpha,\beta,p,q \in \R$ sont appellés éléments simples de
  seconde espèce.
\end{definition}

Pour déterminer les éléments simples dans $\R(X)$, on peut :
\begin{itemize}
  \item déterminer les élements simples dans $\C(X)$ puis regrouper les
    «pôles conjugués» (à éviter en général) ;
  \item procéder par identification en utilisant les propriétés de la
    fonction rationnelle (parité, …)
\end{itemize}

\section{Applications}

Calcul de dérivées, de primitives (Gourdon analyse 137), somme de série
entières (Gourdon analyse 240)


Séries génératrices et partition d'un entier, ou dénombrement des
solutions d'une équation diophantienne



\end{document}
