\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{im}}
\newcommand{\Id}{\mathop{Id}}


\parindent0pt

\usepackage{babel}

\title{153 Polynômes d’endomorphisme en dimension finie. Réduction d’un
endomorphisme en dimension finie. Applications}
\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

Dans cette leçon, $\K$ désigne un corps commutatif.

\section{Généralités}

On cherche à exhiber les classes de similitudes de bases d'un espace
vectoriel. Il s'agit en fait des classes d'équivalences de matrices
inversibles pour la similitude.  On cherche à en exhiber des
représentants «simples».

\begin{theoreme}
  Soient $u$ un endomorphisme de $E$, et $E'$ un sous-espace de $E$
  stable par $u$. Alors l'application $u'$ induite par $u$ sur $E'$ est
  un endomorphisme de $E'$.
\end{theoreme}

\begin{proposition}
  La matrice qui représente l'endomorphisme $u$ de $E$ dans une base $e
  = (e_1,\dots,e_n)$ est diagonale si et seulement si tout vecteur de la
  base est transformé par $u$ en un vecteur colinéaire.
\end{proposition}

\begin{definition}
  Soit $u$ un endomorphisme de $E$. Toutes les fois qu'il existe un
  scalaire $\lambda$ de $\K$ et un vecteur non nul de $x$ de $E$ tels
  que $u(x) = \lambda x$ ; on dit que $\lambda$ est une valeur propre de
  $u$ et que $x$ est un vecteur propre de $u$ associé à $\lambda$.
\end{definition}

\begin{definition}
  L'ensemble des valeurs propres d'un endomorphisme $u$ s'appelle le
  \emph{spectre} de $u$ et est noté $\Sp u$.
\end{definition}

\begin{theoreme}
  Soient $u$ un endomorphisme de $E$, $\lambda$ un scalaire de $\K$, et
  $E(\lambda)$ le noyau de l'endomorphisme $u - \lambda \Id$ de $E$,
  (sous-espace vectoriel de $E$).

  Pour que $\lambda$ soit valeur propre de $u$, il faut et il suffit que
  $u - \lambda \Id$ ne soit pas injectif , c'est-à-dire que $E(\lambda)
  \neq \{0\}$.
\end{theoreme}

\begin{definition}
  Soient $u$ un endomorphisme de $E$ et $\lambda$ une valeur propre de
  $u$. Le sous espace vectoriel $E(\lambda) = \ker(u - \lambda \Id)$ de
  $E$ est dit sous espace propre de $u$ associé à  $\lambda$
\end{definition}

\begin{theoreme}
  Soient $u$ un endomorphisme de $E$, et $(\lambda_i)_{1\leqslant i
  \leqslant n}$ une famille finie de valeurs propres deux à deux
  distinctes de $u$. Alors la somme $E_1 + \cdots + E_n$ des
  sous-espaces propres associés est directe.
\end{theoreme}

\begin{corollaire}
  Soit $u \in \mathcal{L}(E)$. Alors la somme des sous-espaces propres
  $\ker(u - \lambda \Id)$ où $\lambda$ parcourt le spectre de $u$, est
  directe.
\end{corollaire}

\begin{definition}
  Lorsque la somme précédente coïncide avec $E$, on dit que
  l'endomorphisme  est diagonalisable.
\end{definition}

\begin{theoreme}
  Soient $u$ et $v$ deux endomorphismes de $E$ qui commuttent. Alors :
  \begin{enumerate}[label=(\roman*)]
    \item Tout sous-espace propre relativement à $u$ est stable par $v$
      ;
    \item Le noyau et l'image de $u$ sont stables par $v$.
  \end{enumerate}
\end{theoreme}

Ce théoèrme pose les prémisses de la diagonalisation simultanée des
endomorphismes.

Par la suite, on étudie un endomorphismes $u$ d'un $\K$ espace-vectoriel
de dimension finie non nulle $n$. On associe, dans une base $e$
abritrairement choisie la matrice $M = [\alpha_{ij}] \in
\mathcal{M}_n(\K)$.

\begin{definition}
  On appelle \emph{polynôme caractéristique de $M$} et on note $\chi_M$
  le déterminant de la $\K[X]$ matrice $XI_n - M$.
\end{definition}

\begin{proposition}
  Une matrice et sa transposée ont même polynôme caractéristique.
\end{proposition}

\begin{theoreme}
  Soit $u$ un endomorphisme de $E$ un espace-vectoriel de dimension
  finie non nulle $n$. Alors le polynôme caractéristique de la matrice
  $M$ réprésentant $u$ est indépendant de la base choisie.
\end{theoreme}

\begin{definition}
  On parle alors du \emph{polynôme caractéristique} de $u$ et on le note
  $\chi_u$.
\end{definition}

\begin{corollaire}
  Un endomorphisme $u$ d'un espace vectoriel de dimension finie non
  nulle $n$ sur un corps $\K$ algébriquement clos possède au moins une
  valeur propre.
\end{corollaire}

\begin{proposition}
  Les endomorphisme $u$ et $\vphantom{e}^tu$ ont même polynôme
  caractéristique.
\end{proposition}

\begin{proposition}
  Soient $u$ un endomorphisme de $E$ espace vectoriel de dimension finie
  non nulle $n$ et $E'$ un sous-espace vectoriel de $E$, stable par $u$
  de dimension $n' > 0$.

  Alors le polynôme de l'endomorphisme $u'$ de $E'$ induit par $u$
  divise le polyôme caractéristique de $u$.
\end{proposition}

\begin{proposition}
  Soient $u$ un endomorphisme de $E$ $\K$ espace vectoriel de dimension
  finie non nulle $n$, $\lambda$ une valeur propre de $u$, $\nu$ l'ordre
  de multiplicité de $\lambda$ et $p$ la dimension du sous-espace propre
  $E_{\lambda}$. Alors : \[ 1 \leqslant p \leqslant \nu \]
\end{proposition}

\begin{theoreme}
  Soit $u$ un endomorphisme de $E$ $\K$ espace vectoriel de dimension
  finie non nulle $n$. Les trois assertions suivantes sont équivalents :
  \begin{enumerate}[label=(\roman*)]
    \item $u$ est diagonalisable ;
    \item il existe une base $e$ de $E$ dans laquelle $u$ est réprésenté
      par une matrice diagonale ;
    \item le polyôme caractéristique $\chi_u$ de $u$ est scindé sur $\K$
      et, pout tout valeur propre de $u$, la dimension du sous-espace
      propre associé est égale à la multiplicité.
  \end{enumerate}
\end{theoreme}

\textsc{Cas particulier :} Tout endomorphisme $u$ d'un $\K$
espace-vectoriel de dimension finie non nulle $n$ admettant $n$ valeurs
propres est diagonalisable.

\section{Polynômes d'endomorphismes}

Définir puissance d'un endomorphisme ;

inclusion des noyaux itérés : $\{0\} \subset \ker u \subset u^2 \dots
\subset \ker u^r = E$

\begin{theoreme}
  L'application $\varphi_u$ qui au polyôme $P$ de $\K[X]$ associe
  l'endomorphisme $P(u)$ est un morphisme de la $\K$-algèbre $\K[X]$
  dans $\mathcal{L}(E)$.
\end{theoreme}

\begin{corollaire}
  L'image de $\K[X]$ par le morphisme d'algèbre $\varphi_u$ est la
  sous-algèbre commutative de $\mathcal{L}[E)$ engendrée par $u$.
\end{corollaire}

\begin{definition}
  On appelle cette sous-algèbre \emph{algèbre des polyômes de
  l'endomorphisme $u$} et on la note $\K[u]$.
\end{definition}

\begin{corollaire} Le noyau de $\varphi_u$ est un idéal de $\K[X]$.
\end{corollaire}

\begin{definition}
  On l'appelle \emph{idéal annulateur de $u$}, et on le note
  $\mathcal{I}_u$.
\end{definition}

\begin{theoreme}
  Soit $u$ un endomorphisme dont le polynôme annulateur $\mathcal{I}_u$
  n'est pas réduit au polynôme nul. Alors il existe un, et un seul
  polynôme unitaire qui engendre $\mathcal{I}_u$.
\end{theoreme}

\begin{definition}
  On appelle \emph{polynôme minimal} le polynôme annulateur unitaire qui
  engendre $\mathcal{I}_u$. On le note $\mu_u$.
\end{definition}

\begin{proposition}
  Tout endomorphisme $u$ d'un $\K$-espace vectoriel $E$ de dimension
  finie non nulle $n$ possède un polyôme minimal.
\end{proposition}

\begin{proposition}
  Soient $u$ un endomorphisme d'un $\K$-espace vectoriel de dimension
  finie non nulle $n$ admettant $\mu_u$ comme polyôme minimal et $E'$ un
  sous-espace vectoriel stable par $u$. Alors l'endomorphisme $u'$
  induit par $u$ sur $E'$ admet un polyôme minimal et $\mu_{u'}$ divise
  $\mu_u$.
\end{proposition}

\begin{proposition}
  Pour tout $P\in \K[X]$, $\ker P(u)$ et $\im P(u)$ sont stables par
  $u$.
\end{proposition}

\begin{proposition}
  Pour tout $(P,\lambda,q) \in \K[X] \times \K \times \mathbf{N}$ : \[
    \ker (u - \lambda \Id)^q \subset \ker (P(u)) - P(\lambda)Id)^q \]
\end{proposition}

\end{document}
