\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}


\title{$\SO_3(\R)$ et les quaternions}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}
  Soit $\H$ le corps non commutatif des quaternions.

  Le centralisateur $Z(\H)$ du groupe multiplicatif des quaternions (vu
  comme un $\R$-espace vectoriel) est $Z(\H) = \R$.
\end{theoreme}

\begin{theoreme}
  Soit $\H$ le corps non commutatif des quaternions.

  Soit $G$ le groupe des quaternions de module 1 (c'est-à-dire $ \{ a +
  bi + cj + dk \mid a^2 + b^2 + c^2 + d^2 = 1 \}$ )

  On a un isomorphisme $\overline{s} \colon G / \{ -1, 1 \}
  \overset{\sim}{\to} \SO_3(\R)$
\end{theoreme}

\section*{Preuve: on fait agir $G$ sur $\H$ par automorphismes
intérieurs}

$G \overset{S}{\curvearrowright} H$ par $\forall q \in G$, \[
  \begin{array}{rcl} S_q \colon H & \to & H \\
  q' & \mapsto & qq'q^{-1} = qq'\overline{q} \text{ car } q\in G
\end{array} \]

On a bien défini une action car $S_q$ est clairement $\R$-linéaire et
bijective ($S_q^{-1} = S_{\overline{q}}$) et on a ainsi une application
$\begin{array}{rcl} S \colon G & \to & \GL_4(\R) \\ q & \mapsto & S_q
\end{array}$.

Si $q_1, q_2 \in G, q'\in \H, S_{q_1q_2}(q') = (q_1q_2) q'
(\overline{q_1q_2}) = q_1 (q_2 q' \overline{q_2}) \overline{q_1} =
S_{q_1}(S_{q_2}(q')) = S_{q_1} \circ S_{q_2}(q')$.

Donc $S$ est un homomorphisme.

Si $S_q = \Id$, alors $\forall q' \in \H, S_q(q') = q' \iff qq' = q'q
\iff q \in Z(\H) \cap G = \R \cap G = \{ -1, 1 \}$. Donc $\ker S_q = \{
-1, 1\}$.

\subsection*{$S_q$ conserve la norme $N$ si $q \in G$}
En effet, si $q' \in \H, N(S_q(q')) = N(q q' \overline{q}) = N(q) N(q')
N(\overline{q}) = N(q')$ car $N(q) = 1$ par définition. Donc $\forall q
\in G, S_q$ est un élément du groupe orthogonal de $(\R^4,N)$,
c'est-à-dire un élément de $\O_4(\R)$.

Notons $P = \{ bi + cj + dk ; (b,c,d) \in \R^3 \}$ l'ensemble des
quaternions «purs». Pour le produit scalaire $\langle q_1, q_2 \rangle =
\frac{1}2 \left( N(q_1 + q_2)^2 - N(q_1)^2 - N(q_2)^2 \right)$, $P =
\R^{\perp}$ en considérant que $\R \subset \H$.

$S_q$ peut donc se projeter en $S_q|_{\R}$ et $S_q|_P$.
\begin{itemize}
  \item $S_q|_{\R} = \Id_{\R}$ car $S_q(x) = qxq^{-1} = x$ ($Z(\H) =
    \R$).
  \item $S_q$ laisse stable $P$ car $S_q \in \O_4(\R)$.
  \item On peut alors poser $s_q = S_q|_P$ et c'est donc un élément de
    $\O(P,N|_P) \cong \O_3(\R)$
\end{itemize}

\subsection*{On considère donc le morphisme $s \colon G \to \O_3(\R)$
qui est bien un homomorphisme de noyau $\{-1, 1\}$}

\subsubsection*{$s$ est continue}

En effet, si $q = a + bi + cj + dk$, on peut calculer les coefficients
de la matrice de $s$ dans la base $(i;j;k)$, par exemple $s_q(i) =
qi\overline{q} = (ai - b -ck + dj)(a - bi -cj -dk) = (ab -ab -cd +dc)
+(a^2 + b^2 - c^2 - d^2)i + 2(ad +bc)j + 2(-ac+bd)k$

Les coefficients de $s_q$ sont donc des polynômes homogènes de degré 2.
Si on munit $\O_3(\R)$ de sa topologie «naturelle», $s_q$ est bien
continue.

De plus, $\det$ est une application continue donc $\det \circ s_q$ l'est
aussi et $G$ est connexe (comme sphère unité de $\R^4$.) DOnc $\det
\circ s_q (G)$ l'est aussi et est inclus dans $\{ -1, 1 \}$. L'image de
$s(G)$ par le déterminant est donc un singleton et $\det(s(1)) =
\det(\Id) = 1$. Donc l'image de $s$ est incluse dans $\SO_3(\R)$.

\subsubsection*{$s$ est surjective}
On va montrer qu'on a tous les renversements dans $s(G)$ et comme
ceux-ci engendrent $\SO_3(\R)$ on aura la surjectivité de $s$.

Si $p \in P\cap G$, $s_p(p) = p$. $s_p$ fixe l'axe $\Vect_{\R}(p)$ et
est donc une rotation d'axe $\langle p \rangle$. Or $p \in P$, donc $p^2
= p \times (-\overline{p}) = -p\overline{p} = -1$. Donc $s_{p^2} =
s_{-1} = \Id = (s_p)^2$. Donc $s_p$ est une involution, donc c'est le
renversement d'axe $\Vect_{\R}(p)$. Donc $\forall p \in P$, le
renversement d'axe $\Vect_{\R}(p)$ est dans l'image de $s$, c'est-à-dire
que $s(G)$ contient tous les renversments.

En passant au quotient par le noyau $\ker s = \{ -1, 1\}$ on obtient
bien un isomorphisme de $G/\{ -1, 1\}$ sur $\SO_3(\R)$. \qed

\begin{remarque}
  Les rotations sont souvent représentées et calculées avec des
  quaternions dans les jeux vidéos.
\end{remarque}

\end{document}
