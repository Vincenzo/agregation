\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\title{Inégalité isopérimétrique}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{lemme}
  Soit $f$ une fonction de classe $\Co^1$ sur $[0;1]$ à valeurs dans
  $\C$ telle que $\int_0^1 f = 0$ et $f(0) = f(1)$. Alors \[ \int_0^1
  \lvert f\rvert^2 \leq \frac1{4\pi^2}\int_0^1 \lvert f'\rvert^2 \]
\end{lemme}

\begin{theoreme}[inégalité isopérimétrique]
  Soit $\Gamma$ une courbe du plan, régulière, de classe $\Co^1$
  fermée, sans points multiples. Soient $\mathcal{L}$ sa longueur et
  $\mathcal{A}$ l'aire quelle délimite. Alors \[ 4\pi\mathcal{A} \leq
  \mathcal{L}^2 \]
\end{theoreme}

\section*{Preuve}

\begin{proof}
  On prolonge $f$ en une fonction $1$-périodique de classe $\Co^1$ par
  morceaux.

  Une telle fonction admet un développement série de Fourier dont les
  coefficients sont \[ c_n(f) = \int_0^1 f(t)e^{-in2\pi t}\diff{t} \] Et en
  particulier, on a $c_0(f) = 0$.

  Sa dérivée $f'$ admet également un développement en série de Fourier,
  donné par $c_n(f') = -i2n\pi c_n(f)$.

  Comme $f'$ est continue on peut utiliser l'éaglité de Parseval : \[
  \int_0^1 f'^2(t) \diff{t} = \sum_{n\in\Z} c_n(f')^2 =
    4\pi^2\sum_{n\in\Z}nc_n(f) \geq 4\pi^2\sum_{n\in\Z}c_n(f) = 4\pi^2
  \int_0^1 f^2(t) \diff{t} \]
\end{proof}

\begin{proof}
  Soit $\varphi$, de classe $\Co^1$ un paramétrage admissible de $f$
  telle que $\Gamma$ soit décrite par $f\circ\varphi$ pour $s\in[0;1]$.

  L'aire du domaine $\Omega$ limité par $\Gamma$ peut s'exprimer comme
  $\iint_\Omega 1 \diff{x}\diff{y}$.

  Il faut donc trouver deux fonctions $P$ et $Q$ telles que
  $\frac{\partial P}{\partial x} - \frac{\partial Q}{\partial y} = 1$, ce
  qui permettront d'appliquer la formule de Green-Riemann : \[
    \iint_{\Omega} \frac{\partial Q}{\partial x} -
    \frac{\partial P}{\partial y} \diff{x}\diff{y} = \int_{\partial\Omega} P
  \diff{x} + Q \diff{y} \]

  Prenons $Q(x,y) = \frac{x}2 $ et $P(x,y) = -\frac{y}2$. Ainsi, on peut
  exprimer l'aire de $\Omega$ par $\frac12 \int xy' + x'y \diff{t}$.

  On rappelle que $f = x\vec{\imath} + y\vec{\jmath}$ et donc on peut
  l'écrire en notation complexe $f = x + iy$

  Calculons $\int f\overline{f'}$

  $f\overline{f'} = (x + iy)(x' - iy') = xx' + yy' +i(xy' - x'y) $

  $\int f\overline{f'} = \int xx' + yy' + i\int xy' - x'y) $

  La première intégrale est nulle, d'après la condition $f(0) = f(1)$.

  On a donc que $2\mathcal{A} = \int \lvert f\overline{f'} \rvert$.

  En utilisant l'inégalité de Cauchy-Schwarz, on a $2\mathcal{A} \leq
  \sqrt{\int \lvert f \rvert^2 \int \lvert f' \rvert^2}$.

  On est ici dans les conditions du lemme : \[ \mathcal{A} \leq
  \frac1{2\pi}\int \lvert f' \rvert^2 \]

  Cas d'égalité : Si $\Gamma$ est un cercle, il y'a clairement égalité.

  Réciproquement, s'il y'a égalité, il est nécessaire qu'il y ait
  égalité dans l'inégalité de Cauchy-Shwarz, ce qui entraine que $\lvert
  f\rvert$ et $\lvert f'\rvert$ sont liées. Dans ce cas $\lvert
  f\rvert$ est constante et $\Gamma$ est fermée, donc c'est un cercle.
\end{proof}

\end{document}
