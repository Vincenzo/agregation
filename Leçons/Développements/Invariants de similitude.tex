\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}


\title{Invariants de similitude}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

Soit $E$ un $\K$-espace vectoriel de dimension finie et $f \in \L(E)$.

\begin{notation}
  On note $\Pi_f$ le polynôme miniaml de $f$ et \[ \L_f = \{ P(f) : P
  \in \K[X] \} . \] Si $x \in E$, on note $P_x$ le polynôme unitaire
  engendrant l'idéal $\{ P \in \K[X] : P(f)(x) = 0 \} $ et \[ E_x = \{
  P(f)(x) : P \in \K[X] \} . \]
\end{notation}

\begin{definition}
  Soit $P = X^p + a_{p-1} X^{p-1} + \cdots a_0 \in \K[X]$. On appelle
  \emph{matrice compagnon} de $P$ la matrice \[ C(P) =
    \left ( \begin{matrix}
        0      & \dots  & \dots  & 0      & -a_0     \\
        1      & 0      &        & \vdots & -a_1     \\
        0      & 1      & \ddots & \vdots & \vdots   \\
        \vdots & \ddots & \ddots & 0      & -a_{p-2} \\
        0      & \dots  & 0      & 1      & -a_{p-1} \\
    \end{matrix} \right )
  \]
\end{definition}

\begin{lemme}
  Il existe $x \in E$ tel que $P_x = \Pi_f$.
\end{lemme}

\begin{lemme}~\\[-7mm]\label{lem:dimse}
  \begin{itemize}
    \item Si $k = \deg(\Pi_f)$, l'ensemble $\L_f$ est un sous espace
      vectoriel de $\L(E)$ de dimension $k$, dont une base est $(\Id, f,
      \dots, f^{k-1})$.
    \item Si $l = \deg(P_x)$, l'ensemble $E_x$ est un sous espace
      vectoriel de $E$ de dimension $l$, dont une base est $(x, f(x),
      \dots, f^{l-1}(x))$.
  \end{itemize}
\end{lemme}

\begin{theoreme} \label{thm:invarsimil}
  Soit $f \in \L(E)$, alors il existe $F_1, \dots, F_r$ des sous espaces
  vectoriels de $E$ stables par $f$ tels que
  \begin{enumerate}
    \item $E = F_1 \oplus \cdots \oplus F_r$.
    \item Pour tout $i \in \llbracket 1, r \rrbracket, f|_{F_i}$ est un
      endomorphisme cyclique.
    \item Si $Pi$ désigne le polynôme minimal de $f_i$, alos on a
      $P_{i+1} \mid P_{i}$ pour tout $i \in \llbracket 1, r - 1\rrbracket$.
  \end{enumerate}
  On appelle $P_1, \dots, P_r$ la suite des \emph{invariants de
  similitude}.
\end{theoreme}

\begin{corollaire}\label{cor:matricecomp}
  Si $P_1, \dots, P_r$ désigne la suite des invariants de similitude de
  $f \in \L(E)$, il existe une base $\B$ de $E$ telle que \[ [f]_{\B} =
    \left ( \begin{matrix}
        C(P_1) &        & 0      \\
               & \ddots &        \\
        0      &        & C(P_r) \\
    \end{matrix} \right)
  \]
\end{corollaire}

\section*{Preuve}

\begin{proof}[Lemme \ref{lem:dimse}]
  L'application \begin{align*} \K[X] \to \L(E) \\ P \mapsto P(f)
  \end{align*} est linéaire et son image est $\L_f$. Montrons que
  $(\Id, f, \dots, f^{k-1})$ est une base de $\L_f$.
  \begin{itemize}
    \item Libre

      Soient $\lambda_0, \dots, \lambda_{k-1}$ tels que $\lambda_0 \Id +
      \cdots + \lambda_{k-1}f^{k-1} = 0$.

      Posons $P = \lambda_0 + \cdots \lambda_{k-1} X^{k-1}$, on alors
      $P(f) =0$. Ainsi, $\Pi_f \mid P$. Or $\deg(P) < \deg(\Pi_f)$ donc
      $P = 0$.
    \item Génératrice

      Soit $P(f) \in \L_f$.

      Effectuons la division euclidienne de $P$ par $\Pi_f$. \[ P =
      \Pi_f Q + R \text{ avec } \deg(R) < \deg(\Pi_f) = k . \] Ainsi,
      $P(f) = R(f) \in \Vect(\Id, f, \dots, f^{k-1})$.
  \end{itemize}
\end{proof}

\begin{proof}[Théorème \ref{thm:invarsimil}]
  Soit $k = \deg(\Pi_f)$ et soit $x \in E$ tel que $P_x = \Pi_f$. Le
  sous espace vectoriel $F = E_x$ est de dimension $k$ et il est stable
  par $f$. Comme $\deg(P_x) = k$, la famille de vecteurs \[ e_1 = x,
  e_2, \dots, e_{k} = f^{k-1}(x) \] forme une base de $F$. Complétons
  cette base en une base $(e_1, \dots, e_n)$ de $E$. En désignant par
  $(e_1^*, \dots e_n^*)$ la base duale associée, on note \[ G =
    \Gamma^{\circ} \text{ où } \Gamma = \{ e_k^* \circ f^i, i \in \N \}
    . \] Autrement dit, $G = \{ x \in E : \forall i \in \N, e_k^* \circ
  f^i(x) = 0 \}$. $G$ est un sous espace vectoriel de $E$ stable par
  $f$.

  Montrons $E = F \oplus G$.

  \begin{itemize}[label=$\blacktriangleright$]
    \item Montrons que $F \cap G = \{ 0 \}$.

      Soit $y \in F \cap G$. Si $ y \neq 0$, on peut écrire $y = a_1 e_1
      + \cdots + a_p e_p$, avec $a_p \neq 0$ et $p \leqslant k$. En
      composant par $e_k^* \circ f^{k - p}$, on obtient \begin{align*}
        0 & \underbrace{=}_{y \in G} e_k^* \circ f^{k - p}(y)          \\
          & = e_k^* \circ f^{k - p}(a_1 e_1 + \cdots + a_p e_p)        \\
          & = e_k^*(y_1 f^{k - p}(e_1) + \cdots + y_p f^{k - p}(e_p))  \\
          & = e_k^*(y_1 f^{k - p}(x)) + \cdots e_k^*(y_p f^{k - 1}(x)) \\
          & = e_k^*(y_1 e_{k -p +1} + \cdots + y_p e_k )               \\
          & = y_p                                                      \\
      \end{align*} ce qui est absurde car on a supposé $y \neq 0$
    \item Montrons que $\dim(F) + \dim(G) = n$ (ou que $\dim(G) = n -
      k$).

      On sait que $G = \Gamma^{\circ} = \Vect(\Gamma)^{\circ}$ et $\dim
      (\Vect \Gamma) + \dim (\Vect \Gamma)^{\circ} = n$. Montrons que
      $\dim (\Vect \Gamma) = k$. Posons \begin{align*}
        \phi \colon \L_f & \to           & \Vect \Gamma \\
        g                & \mapsto e_k^* & \circ g      \\
      \end{align*} Par définition de $\Gamma$, $\phi$ est surjective.
      Soit $g \in \ker \phi$. On a alors $e_k^* \circ g = 0$ et, comme
      $g \in \L_f$, \[ g = g_1 \Id + \cdots + g_pf^{p-1} \text{ avec } g_p
        \neq 0 \text{ et } p \leqslant k . \] On a donc $0 = e_k^* \circ g(
      f^{k-p}(x)) = g_p \neq 0$. Ainsi $g = 0$ et $\phi$ est un
      isomorphisme.

      Donc $\dim \Vect \Gamma = \dim \L_f = k$ et on a le résultat.
  \end{itemize}
  Soit $P_1$ le polynôme minimal de $f|_f$ (qui est le polynôme minimal de
  $f$ car $P_1 = P_x = \Pi_f$). Soit $P_2$ le polynôme minimal de $f|_G$.
  Comme $G$ est stable par $f$, on a $P_1(f|_G) = \Pi_f(f|_G) = 0$ et donc
  $P_2 \mid P_1$.

  Par récurrence sur $f|_G$ et à $G$, on obtient le résultat.

\end{proof}

\begin{proof}[Corollaire \ref{cor:matricecomp}]
  Soient $F_1, \dots, F_r$ les sous espaces vectoriels stables par $f$
  donnés par le théorème avec $l_i = \dim F_i$.

  On a $f_i = f|_{F_i}$ cyclique.

  Ainsi, il existe $x_i \in E$ tel que $\B_i = (x_i, f_i(x_i), \dots,
    f_i^{l_i - 1}(x_i))$ soit une base de $F_i$. On a donc \[ [f_i]_{\B_i} =
    C(P_i) \] d'où le résultat en écrivant le résultat dans la base $\B =
    \bigcup_{i=1}^r \B_i$.
\end{proof}

\end{document}
