\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Méthode de Laplace}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}


\begin{theoreme}
  Soient $I = ]a,b[$ un intervalle borné ou non, $\varphi \in
  \Co^2(I,\R)$ et $f \in \Co^0(I,\R)$. On suppose \begin{enumerate}
    \item $\forall t > 0, \int_a^b e^{t\varphi(x)} \lvert f(x) \rvert
      \diff x < +\infty$
    \item $\varphi' $ s'annule en un unique $x_0 \in I$ et
      $\varphi''(x_0) < 0$
    \item $f(x_0) \neq 0$
  \end{enumerate}
  Alors \[ F(t) \underset{t\to +\infty}{\sim} \frac{\sqrt{2\pi}}{\sqrt{
  \lvert \varphi''(x_0)\rvert }} e^{t\varphi(x_0)}f(x_0)t^{-1/2} \] où
  $F(t) = \int_a^b e^{t\varphi(x)} f(x) \diff x$.
\end{theoreme}

\section*{Preuve}

La formule de Taylor avec reste intégral permet de fixer $\psi \colon I
\to \C, \Co^0$ sur $I$, $\Co^2$ sur $I \setminus \{x_0\}$ telle que
$\varphi(x) = \varphi(x_0) + (x - x_0)^2 \psi(x)$ sur $I$, et $\psi(x_0)
= \frac{1}2 \varphi''(x_0)$. On fixe alors $\delta_1 > 0$ tel que $J_1 =
]x_0 - \delta_1, x_0 + \delta_1 [ \subset ]a,b[$ et $\psi(x) < 0$ sur
$J_1$. La fonction $u \colon x \mapsto (x - x_0) \sqrt{ - \psi(x) }$ est
alors $\Co^0$ sur $J_1$ et $\Co^2$ sur $J_1 \setminus \{x_0\}$, et se
prolonge en une fonction $\Co^1$ sur $J_1$, telle que $u'(x_0) =
\sqrt{-\frac{1}2 \varphi''(x_0)}$. En effet, pour $x \neq x_0$, on a
$\psi(x) = \frac{\varphi(x) - \varphi(x_0)}{(x - x_0)^2}$ donc, \[ (x -
x_0)\psi'(x) = \frac{\varphi'(x) - \varphi'(x_0)}{x - x_0} - 2 \psi(x)
\xrightarrow[x \to x_0]{}{0}, \] donc \[ u'(x) = \sqrt{ - \psi(x)} - (x
  - x_0) \frac{\psi'(x)}{2\sqrt{ - \psi(x)}} \xrightarrow[x \to
x_0]{}{\sqrt{-\frac{1}2 \varphi''(x_0)}} . \] Il existe alors $\delta
\leqslant \delta_1$ tel que $u'(x) \neq 0$ pour $\lvert x - x_0 \rvert
\leqslant \delta$.

Soit à présent $\theta \in \Co^{\infty}(]x_0 - \delta, x_0 + \delta [)$
tel que $0 \leqslant \theta \leqslant 1$ et $\theta = 1$ si $\lvert x -
x_0 \rvert \leqslant \frac{\delta}2$.

On écrit $F(t) = F_1(t) + F_2(t)$ avec $F_1(t) = \int_a^b
e^{t\varphi(x)} \theta(x) f(x) \diff x$ et $F_2 (t) = \int_a^b
e^{t\varphi(x)} (1 - \theta(x)) f(x) \diff x$.

\subsection*{Étude de $F_1$}

Comme $\theta$ est nulle en dehors de $]a,b[$, on a $F_1(t) = \int_{\R}
e^{t\varphi(x_0) + t(x - x_0)^2 \psi(x)} \theta(x) f(x) \diff x$.

On pose alors dans l'intégrale ci-dessus, $z = (x - x_0)\sqrt{-
\psi(x)}$, ce qui est valable sur le support de $\theta$. On a alors $x
= g(z)$, avec $g(0) = x_0$, $g'(0) = \frac{1}{\sqrt{- \psi(x)}} =
\frac{1}{\sqrt{- \frac{1}2 \varphi''(x_0)}}$, et $F_1(t) =
e^{t\varphi(x_0)} \int_{\R} e^{-tz^2} h(z) \diff z$, où $h \colon z
\mapsto \theta(g(z)) f(g(z)) g'(z)$ est continue à support compact.

On pose ensuite $y = \sqrt{t}z$, ce qui donne \[ F_1(t) =
  \frac{e^{t\varphi(x_0)}}{\sqrt{t}} \int_{\R} e^{-y^2} h\left(
\frac{y}{\sqrt{t}} \right) \diff y . \] Or $e^{-y^2} h\left(
\frac{y}{\sqrt{t}} \right) \xrightarrow[t \to + \infty]{} e^{-y^2} h(0)$
et $\left \lvert e^{-y^2} h\left( \frac{y}{\sqrt{t}} \right) \right
\rvert \leqslant \sup_{\R} \lvert h \rvert e^{-y^2} \in L^1(\R)$

Par convergence dominée, $\sqrt{t}e^{-t\varphi(x_0)} F_1(t)
\xrightarrow[t\to +\infty]{} h(0) \int_{\R} e^{-y^2} \diff y = h(0)
\sqrt{\pi}$.

De plus, $h(0) = \theta(x_0)f(x_0)g'(0) = \frac{f(x_0)}{\sqrt{-\frac{1}2
\varphi''(x_0)}}$, donc \[ F_1(t) \underset{t\to + \infty}{\sim}
  \frac{\sqrt{-2\pi}}{\sqrt{ t \varphi''(x_0)}}
e^{t\varphi(x_0)}f(x_0) \]

\subsection*{Étude de $F_2$}
Sur le support de $1 - \theta$, on a $\lvert x - x_0 \rvert \geqslant
\frac{\delta}2$. Comme $\varphi'$ ne s'annule qu'en $x_0$ et que $x_0$
est un maximum, on a $\varphi'(x) > 0$ pour $x < x_0$ et $\varphi'(x) <
0$ pour $x > x_0$. On a alors \begin{align*} \varphi(x_0) - \varphi(x) &
= \varphi(x_0) - \varphi\left( x_0 - \frac{\delta}2 \right) +
\varphi\left( x_0 - \frac{\delta}2 \right)- \varphi(x) \\ & \geqslant
\varphi(x_0) - \varphi\left( x_0 - \frac{\delta}2 \right) > 0 \text{ si
} x - x_0 < \frac{\delta}2 \\ \varphi(x_0) - \varphi(x) &
= \varphi(x_0) - \varphi\left( x_0 + \frac{\delta}2 \right) +
\varphi\left( x_0 + \frac{\delta}2 \right)- \varphi(x) \\ & \geqslant
\varphi(x_0) - \varphi\left( x_0 + \frac{\delta}2 \right) > 0 \text{ si
} x - x_0 > \frac{\delta}2 \end{align*} Donc sur le support de $1 -
\theta$, on a $\varphi(x_0) - \varphi(x) \geqslant \mu > 0$.

Alors pour $t > 1, t\varphi(x) = \varphi(x) + (t - 1)\varphi(x)
\leqslant \varphi(x) + (t - 1)\varphi(x_0) - (t - 1) \mu$. Donc $\lvert
F_2(t) \rvert \leqslant e^{t\varphi(x_0)} \int_a^b e^{\varphi(x)} \lvert
f(x) \rvert \diff x e^{-\varphi(x_0) -(t-1)\mu}$ donc $\lvert F_2(t)
\rvert e^{-t\varphi(x_0)} \leqslant M e^{-t \mu}$, où $M = e^{\mu -
\varphi(x_0)} \int_a^b e^{\varphi(x_0)} \lvert f(x) \rvert \diff x < +
\infty$ donc $\lvert F_2(t) \rvert e^{-t\varphi(x_0)}$ est à
décroissance exponentielle, alors que $e^{-t\varphi(x_0)} F_1(t)$
décroit en $\frac{1}{\sqrt{t}}$, donc $F_2(t) = o\,(F_1(t))$, d'où \[
  F(t) \underset{t\to + \infty}{\sim} \frac{\sqrt{2\pi}}{\sqrt{ \lvert
\varphi''(x_0)\rvert}} e^{t\varphi(x_0)}f(x_0) t^{-1/2} .\] \qed

\subsection*{Application : formule de Stirling}

On a $\Gamma(t+1) = \int_0^{+\infty} x^t e^{-x} \diff x = \int_0^{+\infty}
e^{ t \ln(x) - x }\diff x = t e^{t \ln t} \int_0^{+\infty} e^{t(\ln y -
y)} \diff y$ en posant $x = ty$.

On a : \begin{enumerate}
  \item $\int_0^{+\infty} e^{t(\ln y - y)} \diff y = \int_0^{+\infty}
    y^t e^{-ty} \diff y < + \infty$
  \item $\varphi'(y ) = \frac{1}y - y = 0 \iff y = 1$, et $\varphi''(1)
    = 1$.
  \item $f \equiv 1$
\end{enumerate}

Donc $\Gamma(t+1) \underset{t\to + \infty}{\sim} \sqrt{2\pi} t^{t +
\frac{1}2} e^{-t}$ et en particulier : \[ n! \underset{t\to +
  \infty}{\sim} \sqrt{2\pi} n^{n + \frac{1}2} e^{-n} = \sqrt{2\pi n}
\left( \frac{n}e \right)^n . \] \qed

\end{document}
