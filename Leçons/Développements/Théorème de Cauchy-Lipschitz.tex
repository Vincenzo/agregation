\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Théorème de Cauchy-Lipschitz}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{definition}
  On dit qu'une application $F$ d'un ouvert $\Omega$ de $\R \times \R^n$
  est \emph{localement lipschitzienne en la seconde variable} si pour
  tout $(t_0,x_0) \in \Omega$, il existe un voisinage $V$ de $(t_0,x_0)$
  et $k > 0$ tels que pour tout $t\in \R$ et pour $x, x' \in \R^n$ tels
  que $(t,x) \in V$ et $(t,x') \in V$, \[ \lVert F(t,x) - F(t,x') \rVert
  \leqslant k \lVert x - x' \rVert . \]
\end{definition}

\begin{definition}
  On se donne une équation différentielle \[ (E) : y^{(n)} =
  F(t,y,y',\dots ,y^{(n-1)}) \] et $(t_0,x_0,\dots , x_{n-1}) \in
  \Omega$. On appelle \emph{problème de Cauchy} de $(E)$ en
  $(t_0,x_0,\dots , x_{n-1})$ la recherche d'une fonction $ \phi \colon
  I \to E$ où $I$ est un intervalle de $\R$ solution de $(E)$ et
  vérifiant $t_0 \in I, \phi(t_0) = x_0, \dots , \phi^{(n-1)}(t_0) =
  x_{n-1}$.
\end{definition}

\begin{lemme}
  On considère l'équation différentielle \[ (E) : y' = f(t,y) \] où $f
  \colon U \to \R^m$ est continue et $U$ est un ouvert de $\R \times
  \R^m$. Une fonction $y \colon I \to \R^m$ est une solution du problème
  de Cauchy de données initiales $(t_0,x_0)$ si et seulement si
  \begin{enumerate}
    \item $y$ est continue et, pour tout $t \in I, (t,y(t)) \in U$ ;
    \item pour tout $t \in I, y(t) = y_0 + \int_{t_0}^t f(u,y(u)) \diff u
      $ ;
  \end{enumerate}
\end{lemme}

\begin{theoreme}
  Soit $F \colon \Omega \subset \R \times \R^n \to \R^n$ (où $ \Omega $
  est un ouvert de $\R \times \R^n$ et $F$ une fonction localement
  lipschitzienne en la seconde variable). Alors pour tout $(t_0,x_0) \in
  \Omega$, il existe un intervalle $I$  voisinage de $t_0$ dans $\R$ et
  une application $\phi \colon I \to \R^n$ solution de $y' = F(t,y)$
  telle que $\phi(t_0) = x_0$. De plus, il y a unicité pour le problème
  de Cauchy de cette équation différentielle en $(t_0,x_0)$.
\end{theoreme}

\section*{Preuve}

\subsection*{Théorème 4}
Soit $(t_0,x_0) \in \Omega$. Pour tout $r > 0$, on pose \[ B_r = \{ x
\in \R^n \mid \lVert x - x_0 \rVert \leqslant r \} \] et pour tout
$\alpha > 0, I_{\alpha} = ] t_0 - \alpha ; t + \alpha [$. Soit $V$ un
voisinage compact de $(t_0, x_0)$ dans $\Omega$ sur lequel $F$ est
$k$-lipschitzienne en la seconde variable. Notons $M$ un majorant de $F$
sur $V$. On peut choisir $r > 0$ et $\alpha > 0$ désormais fixés, tels
que $I_{\alpha} \times B_r \subset V$ et $\alpha M < r$. La fonction $F$
est continue (car lipschitzienne), toute solution est de classe $\Co^1$.
Ainsi, une application $\phi \colon I_{\alpha} \to \R^n$ est solution si
et seulement si pour tout $i \in I, (t, \phi(t)) \in \Omega$ et $\phi(t)
= x_0 + \int_{t_0}^t f(u,y(u)) \diff u$.

Posons $\Fe = \Co(I_{\alpha}, B_r)$, muni de $\lVert \cdot
\rVert_{\infty}$. On sait que $I_{\alpha}$ est un compact et que $B_r$
est complet (comme fermé dans un complet) donc $(\Fe, \lVert \cdot
\rVert_{\infty})$ est complet.

Posons, pour $t \in I_{\alpha}$, \[ \begin{array}{rcl} \phi \colon \Fe &
\to & \Fe \\ y(t) & \mapsto & \left(t \mapsto x_0 + \int_t^{t_0}
f(u,y(u)) \diff u \right) . \end{array} \] $y$ est une solution de $(E)$
si et seulement si $y$ est un point fixe de $\phi$. On va donc vouloir
appliquer le théorème du point fixe. Montrons tout d'abord que si $y \in
\Fe$, alors $\phi(y) \in \Fe$.

Soit $(t_n)_{n \in \N^*}$ une suite de $I_{\alpha}$ convergent vers $t$.
Montrons que $\phi(y)(t_n) \xrightarrow[n \to \infty]{} \phi(y)(t)$.
\begin{align*} \phi(y)(t_n) & = x_0 + \int_t^{t_0} f(u,y(u)) \diff u \\
                            & = x_0 + \int_{\R} \underbrace{ f(u,y(u))
                            \car_{[\min(t_0,t_n),\max(t_0,t_n)]}(u)}_{g_n(u)}
                          \diff u \end{align*}
Par le théorème de convergence dominée, on obtient bien le résultat.

Montrons maintenant que, pour tout $t \in I_{\alpha}, \phi(y)(t) \in
B_r$. \begin{align*}
  \lVert \phi(y)(t) - x_0 \rVert & = \left\lVert \int_t^{t_0} f(u,y(u))
\diff u \right\rVert \\ & \leqslant \int_t^{t_0} \lVert f(u,y(u)) \rVert
  \diff u \\ & \leqslant \lvert t - t_0 \rvert M \\ & \leqslant \alpha M
\\ & \leqslant r \end{align*} On a bien $\phi(y) \in \Fe$. Montrons que
pour tout $y, z \in \Fe$ et pour tout $t \in I_{\alpha}$, \[
  \lVert \phi^p(y)(t) - \phi^p(z)(t) \rVert \leqslant \frac{k^p \lvert
t - t_0 \rvert^p}{p!} \lVert x - y \rVert_{\infty} . \]

Démontrons ceci par récurrence :
\begin{itemize}
  \item Pour $p = 0$ on a bien le résultat.
  \item Supposons le résutalt vrai pour $p$ et montrons le pour $p+1$.
    \[\begin{array}{rcl}
        \lVert \phi^{p+1}(y)(t) - \phi^{p+1}(z)(t) \rVert & = &\left\lVert
      \int_t^{t_0} f(u,\phi^p(y)(u)) \diff u - \int_t^{t_0}
      f(u,\phi^p(z)(u)) \diff u \right\rVert \\
      & \underbrace{\leqslant}_{f\ k-\text{lipschitzienne}} & \left\lvert
      \int_t^{t_0} k \rVert \phi^p(y)(u) - \phi^p(z)(u) \rVert \diff u
      \right\rvert \\
      & \underbrace{\leqslant}_{\text{H.R.}} & \left\lvert
    \int_t^{t_0} k \frac{k^p \lvert u - t_0 \rvert^p}{p!} \lVert y - z
  \rVert_{\infty} \diff u \right\rvert \\ & \leqslant & \frac{k^{p+1}
      \lvert u - t_0 \rvert^{p+1}}{(p+1)!} \lVert y - z  \rVert_{\infty}
  \end{array} \]
\end{itemize}

On obtient donc en particulier que, pour tout $t \in I_{\alpha}$, pour
tout $y,z \in \Fe$ et pour tout $p \in \N$, \[ \lVert \phi^p(y)(t) -
  \phi^p(z)(t) \rVert \leqslant \frac{k^p \alpha^p} \lVert x - y
\rVert_{\infty} . \]

Ainsi \[ \lVert \phi^p(y) - \phi^p(z) \rVert_{\infty} \leqslant
\frac{k^p \alpha^p}{p!} \lVert x - y \rVert_{\infty} . \] On sait que $
\frac{k^p \alpha^p}{p!} \xrightarrow[p \to \infty]{} 0$ comme terme
général d'une série exponentielle. Il existe donc $p_0 \in \N$ tel que
$\frac{k^p \alpha^p}{p!} < 1$. On a donc que $\phi^{p_0}$ est une
application $\frac{k^p \alpha^p}{p!}$-contractante sur $\Fe$. D'après le
théorème du point fixe de Banach-Picard, il existe un unique $y \in \Fe$
telle que $\phi^p(y) = y$. On a alors \[ \phi^p(\phi(y)) =
\phi(\phi^p(y)) = \phi(y) . \] L'unicité du point fixe implique que
$\phi(y) = y$. L'application $\phi$ admet un unique point fixe d'où le
résultat. \qed

\end{document}
