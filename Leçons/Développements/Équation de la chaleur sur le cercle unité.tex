\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz-cd}
\usepackage{enumitem}


\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}}
\newcommand{\Aut}{\mathop{Aut}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Équation de la chaleur sur le cercle unité}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}
  Soit $\U$ le cercle unité de $\C$ et $u_0 \in L^2 = L^2(\U)$,

  Alors il existe une unique $u \colon \R_+^* \times \U \to \C,
  \mathcal{C}^2$, telle que $\left\{ \begin{array}{l}\partial_t u =
    \Delta_x u \\ u(t,\cdot) \rightarrow u_0 \end{array} \right.$

  De plus, $u$ est $\mathcal{C}^{\infty}$.

  ($\U \cong \R/2\pi\Z $)
\end{theoreme}

\section*{Preuve}

\subsection*{Unicité}

\begin{itemize}
  \item Soit $u$ une telle fonction. Soit $t>0$. $u$ étant
    $\mathcal{C}^1$, sa série de Fourier converge normalement sur $\U$.
    vers $u(t,\cdot)$. Notons $c_n(t) =
    \frac1{2\pi}\int_0^{2\pi}u(t,x)e^{-inx}\mathrm{d}x$ ses coefficients
    de Fourier ($n\in\Z$).
  \item Soient $0 < a < b$ deux réels, $\partial_t u$ est bornée sur
    $[a,b] \times \U$ qui est compact. Ainsi $\lvert \partial_t u(t,x)
    \rvert \leqslant \lVert \partial_t u \rVert_{\infty}$. Cette
    dernière majoration permet d'appliquer les théorèmes de dérivations
    sous l'intégrale pour en déduire que $c_n$ est $\mathcal{C}^1$ sur
    $\R_+^*$ et que $\forall t\in \R_+^*, \forall n \in \N, :$ \[ c'_n =
      \frac1{2\pi} \int_0^{2\pi} \partial_t u (t,x)e^{-inx}\mathrm{d}x =
    \frac1{2\pi} \int_0^{2\pi} \Delta_x u(t,x) e^{-inx}\mathrm{d}x \] ce
    qui après une double intégration par parties donne \[  c'_n =
      \frac{-n^2}{2\pi} \int_0^{2\pi} u(t,x) e^{-inx}\mathrm{d}x = -n^2
    c_n(t) .\] Ainsi, $\forall n\in \Z, \exists \dot{c}_n \in \C, \forall
    t\in \R_+^*, c_n(t) = \dot{c}_n e^{-n^2 t}$.
  \item Montrons que pour tout $n \in \Z$, $\dot{c}_n =  \frac1{2\pi}
    \int_0^{2\pi} u_0((x) e^{-inx}\mathrm{d}x$. L'inégalité de
    Cauchy-Schwarz permet d'écrire que $\forall g \in L^2 \left\lvert
      \frac1{2\pi} \int_0^{2\pi} g(x) e^{-inx}\mathrm{d}x \right\rvert
      \leqslant \lVert g \rVert_2$. Comme $u(t,\cdot) \rightarrow u_0$,
      on a $c_n(t) \rightarrow \dot{c}_n$. \qed
\end{itemize}

\subsection*{Existence}
On pose, pour $n\in \Z$, $u_n(t,x) = c_n(u_0)e^{-n^2t}e^{inx}$
\begin{itemize}
  \item D'après l'égalité de Bessel-Parseval, on a $\sum_{n\in \Z}
    \lvert c_n(u_0) \rvert^2 = \lVert u_0 \rVert_2^2, c_n(u_0) \to 0$ et
    donc en particulier $(c_n(n_0))_{n\in \Z}$ est bornée par $M > 0$.
  \item Soient $0 < a < b$ deux réels, la majoration, avec $\alpha,
    \beta \in \N, \left\lvert \frac{\partial^{\alpha+\beta}
    u_n}{\partial t^{\alpha} \partial x^{\beta}} (t,x) \right\rvert
    \leqslant \lvert n \rvert^{2\alpha + \beta} M e^{-n^2 a}$ pour
    tout $t > a$ et $x\in \U$ et la convergence de la série de terme
    général $\sum_{n\in \Z} \lvert n \rvert^{2\alpha + \beta} e^{-n^2
    a}$ montre que la fonction $u$ est $\mathcal{C}^{\infty}$ sur
    $\R_+^* \times \U$.

    On calcule alors $\partial_t u = \sum_{n\in \Z} \partial_t u_n =
    \sum_{n\in \Z} \partial^2_x u_n = \partial^2_x u$.
  \item La convergence normale de la série définie par $u$ rend licite
    l'échange $\int\sum = \sum\int$ suivant : pour $n \in \Z$, \[
      \frac1{2\pi} \int_0^{2\pi} \sum_{n\in \Z} c_n(u_0) e^{-n^2t}
      e^{-inx} e^{-imx} \mathrm{d}x = \sum_{n\in \Z} c_n(u_0)
      \frac1{2\pi} \int_0^{2\pi} e^{-inx} e^{-imx} \mathrm{d}x =
      c_n(u_0)e^{-m^2t}.\] L'égalité de Bessel-Parseval s'écrit \[
      \lVert u(t,\cdot) - u_0  \rVert_2^2 = \sum_{n\in \Z} \lvert
    c_n(u_0) \rvert^2 \lvert 1 - e^{-n^2t} \rvert^2 \] et la majoration
    $\lvert c_n(u_0) \rvert^2 \lvert 1 - e^{-n^2t} \rvert^2 \leqslant
    \lvert c_n(u_0) \rvert^2$ qui est sommable, donc on peut appliquer
    le théorème de convergence dominée et obtient que
    $\lim_{t\to+\infty} \lVert u(t,\cdot) - u_0  \rVert_2^2 = \sum_{n\in
    \Z} \lvert c_n(u_0) \rvert^2 \lvert 1 - e^{-n^2t} \rvert^2 = 0$ et
    donc le résultat. \qed
\end{itemize}


\end{document}
