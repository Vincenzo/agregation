\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Polynômes cyclotomiques}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{definition}
  Soit $n \in \N^*$. Notons $G_n$ le groupe des inversibles de $\Z/n\Z$.
  On appelle \emph{indicateur d'Euler} de $n$ l'entier $\varphi(n) =
  \Card(G_n)$. On a aussi que $\varphi(n)$ est le nombre d'entier $k \in
  \llbracket 1, n\rrbracket$ tels que $k \wedge n = 1$.
\end{definition}

\begin{proposition}
  Si $G$ est un groupe cyclique d'ordre $n, G = \langle a \rangle$ alors
  \[ \langle a^k \rangle = G \iff k \wedge n = 1 .\]
\end{proposition}

\begin{theoreme}[Lemme de Gauss] Soit $P \in \Z[X]$, on note $c(P)$ le
  pgcd des coefficients de $P$ (et $c(P) = 1$ si $P= 0$). Si $P,Q \in
  \Z[X]$ alors $c(PQ) = c(P)c(Q)$
\end{theoreme}
\begin{proof}
  Posons $P_1 = \frac{1}{c(P)}$ et $Q_1 = \frac{1}{c(Q)}$. On a clairement
  $c(P_1) = c(Q_1) = 1$. Montrons par l'absurde que $c(P_1Q_1) = 1$. Pour
  cela, on suppose que $c(P_1Q_1) > 1$. Dans ce cas, il existe un nombre
  premier $p$ tel que $p \mid c(P_1Q_1)$ et on en tire que $p \mid c(P_1)$
  ou $p \mid c(Q_1)$. ce qui est absurde, donc $c(P_1Q_1) = 1$. On a donc
  $c(P)c(Q)c(P_1Q_1) = c(P)c(Q)$ d'où le résultat.
\end{proof}

\begin{definition}
  Pour tout $n\in N^*$, on note \[ \U_n = \left\{\exp\left(
  \frac{2ik\pi}n\right), k\in \Z \right\} \subset \C .\] On dit qu'un
  élément $x \in \U_n$ est une \emph{racine primitive $n$\up{ième} de
  l'unité} si $x$ engendre le groupe multiplicatif $\U_n$. On note
  $\Pi_n$ l'ensemble des racines primitives $n$\up{ième} de l'unité, et
  on définit le \emph{polynôme cyclotomique d'indice $n$} par \[ \Phi_n
  = \prod_{\xi \in \Pi_n} (X - \xi ) . \]
\end{definition}

\begin{proposition}
  Pour tout $n \in \N^*$, \[X^n - 1 = \prod_{d \mid n} \Phi_d .\] En
  particulier, $\Phi_n \in \Z[X]$.
\end{proposition}
\begin{proof}
  Soit $k \in \llbracket 0, n-1 \rrbracket$ et soit $d$ l'ordre de $w^k$
  dans $\U_n$. On a nécessairement $d \mid n$. Par ailleurs, $(w^k)^d =
  1$, on a donc $(w^k) \in \U_d$ et $w^k$ étant d'ordre $d$, on a $ \lvert
  \langle w^k \rangle \rvert = d = \lvert \U_d \rvert$. On a donc $w^k \in
  \Pi_d$. Ainsi, $(X - w^k)$ divise $\Phi_d$ et par conséquent divise
  $\prod_{d\mid n}\Phi_d$.

  Les valeurs de $w^k$ ($0 \leqslant k \leqslant n - 1$) étant distinctes,
  les polynômes $X - w^k$ sont donc premiers entre eux. On en déduit que
  $X^n - 1 = \prod_{k=0}^{n-1}(X - w^k)$ divise $\prod_{d\mid n}\Phi_d$.
  Ces polynômes étant, de plus, unitaires et de même degré (car
  $\sum_{d\mid n} \deg(\Phi_d) = \sum_{d\mid n} \varphi(d) = n$), alors
  ils sont égaux.

  Montrons maintenant, par récurrence sur $n \in \N^*$, que $\Phi_n \in
  \Z[X]$. Pour $n = 1$, c'est vrai, car $\Phi_1 = X - 1$. Supposons le
  résultat vrai au rang jusqu'au rang $n-1$ et montrons le au rang $n$.
  D'après l'hypothèse de récurrence, le polynôme $P =
  \prod_{\substack{d\mid n \\d \neq n}} \Phi_d \in \Z[X]$. Par ailleurs,
  on a $X^n - 1 = \Phi_n P$. Comme $P$ est unitaire, on peut effectuer la
  division euclidienne de $X^n - 1$ par $P$ dans $\Z[X]$. \[ \exists Q,R
  \in \Z[X], X^n - 1 = PQ + R \text{ avec } \deg(R) < \deg(P) . \] Il y a
  unicité du couple $(Q,R)$ dans $\Z[X]$ (car c'est un anneau intègre)
  donc dans $\C[X]$ et comme $X^n - 1 = \Phi_nP$, on en déduit que $R = 0$
  et $Q = \Phi_n$. Donc $\Phi_n \in \Z[X]$, ce qui achève le raisonnement
  par récurrence.
\end{proof}

\begin{lemme}
  $\overline{\Phi_n}$ n'est pas divisible par le carré d'un polynôme non
  constant.
\end{lemme}
\begin{proof}
  Montrons que dans $\Z/p\Z[X]$, $\overline{\Phi_d}$ n'est divisible par
  le carré d'aucun polynôme non constant.

  Supposons $\overline{\Phi_d} = \overline{Q^2P}$ dans $\Z/p\Z[X]$. Si $R
  = \prod_{\substack{d\mid n \\d \neq n}} \Phi_d \in \Z[X]$, on a $X^n - 1
  = \Phi_nR$ et donc $X^n - \overline{1} = \overline{\Phi_nR} =
  \overline{Q^2S}$ (avec $S = PR$) d'où par dérivation algébrique : \[
    \overline{n}X^{n-1} = 2\overline{QQ'S} + \overline{Q^2S} \text{ donc }
  \overline{Q} \mid \overline{n}X^{n-1} \mid \overline{n}X^n .\] De plus,
  $\overline{Q} \mid \overline{n}X^n - \overline{n}$ donc $\overline{Q}$
  divise la différence, c'est-à-dire $\overline{Q} \mid \overline{n}$. Or
  $p \nmid n$, donc $\overline{n} \neq 0$ et donc $\overline{Q}$ est
  constant.
\end{proof}


\begin{theoreme}
  Les polynômes cyclotomiques sont irréductibles sur $\Q$.
\end{theoreme}
\begin{proof}
  \textbullet Montrons tout d'abord que $\Phi_n = F_1\dots F_r$ avec $F_i
  \in \Z[X]$ unitaires et irréductibles dans $\Q[X]$.

  Soit $\Phi_n = G_1\dots G_r$ la décomposition de $\Phi_n$ en facteurs
  irréductibles unitaires de $\Q[X]$ (qui est factoriel). Pour tout $ i
  \in \llbracket 1, r\rrbracket$, il existe $\alpha_i \in \N^*$ tel que
  $\alpha_iG_i \in \Z[X]$. On a $\alpha_1\dots\alpha_r\Phi_n =
  (\alpha_1G_1) \dots (\alpha_rG_r)$. Utilisons le lemme de Gauss. Comme
  $c(\Phi_n) = 1$ (car $\Phi_n$ est unitaire), on a \[
    \alpha_1\dots\alpha_r = c(\alpha_1\dots\alpha_r\Phi_n) =
  \prod_{i=1}^r c(\alpha_iG_i) .\] Pour tout $i \in \llbracket 1,
  r\rrbracket$, on pose le polynôme $F_i =
  \frac{\alpha_iG_i}{c(\alpha_iG_i)}$ et d'après ce qui précède, on a
  $\Phi_n = F_1\dots F_r$. En effet, \[ F_1\dots F_r = \frac{\prod_{i=1}^r
    \alpha_i}{\prod_{i=1}^r c(\alpha_iG_i)}\prod_{i=1}^r G_i =
  \prod_{i=1}^r G_i = \Phi_n .\] Pour tout $i \in \llbracket 1,
  r\rrbracket$, $F_i \in \Z[X]$ est irréductible dans $\Q[X]$ et est
  forcément unitaire, car $\Phi_n$ l'est.

  \textbullet Soit maintenant $\xi$ une racine de $F_1$ dans $\C$ et $p$
  un nombre premier tel que $p \nmid n$. Montrons qu'il existe $i \in
  \llbracket 1, r \rrbracket$ tel que $F_i(\xi^p) = 0$.

  L'élément $\xi$ est une racine de $F_1$ donc de $\Phi_n$. On a donc $\xi
  \in \Pi_n$. Or $p$ est premier et $p \nmid n$ donc $p \wedge n = 1$ et
  donc $\xi^p \in \Pi_n$, d'où $\xi^p$ est racine de $\Phi_n$. Il existe
  donc $i \in  \llbracket 1, r \rrbracket$ tel que $F_i(\xi^p) = 0$.

  \textbullet Montrons que $i = 1$, c'est-à-dire que $F_1(\xi^p) = 0$.

  Comme $F_i(\xi^p) = 0$, $F_1(X)$ et $F_i(X^P)$ ne sont pas premiers
  entre eux dans $\C[X]$. On a aussi que $F_1(X)$ et $F_i(X^P)$ ne sont
  pas premiers entre eux dans $\Q[X]$ (si c'était le cas, en utilisant le
  théorème de Bezout, il le seraient aussi dans $\C[X]$). Il existe donc
  un facteur commun irréductible et unitaire. De plus, $F_1(X)$ est
  irréductible dans $\Q[X]$ donc $F_1(X) \mid F_i(X^p)$ et cette
  divisibilité s'étend à $\Z[X]$. On en déduit que $F_1(X) \mid F_i(X^p) =
  F_i(X)^p$. Soit $\overline{P} \in \Z/p\Z[X]$ un facteur irréductible de
  $\overline{F_1}$ dans $\Z/p\Z[X]$. On a $\overline{P} \mid
  \overline{F_i}(X)^p$ donc $\overline{P} \mid  \overline{F_i}(X)$. Par
  conséquent, si $i \neq 1$, on devrait avoir $\overline{P} \mid
  \overline{\Phi_n}$, ce qui n'est pas possible.

  \textbullet Montrons que pour tout entier $k$ premier avec $n$,
  $F_1(\xi^k) = 0$.

  Écrivons $ k = p_1 \dots p_s$, les $p_i$ étant des nombres premiers.
  Prouvons par récurrence sur $s$ que $F_1(\xi^k) = 0$. Pour $s = 1$, on
  est ramené au cas précèdent. Supposons le résultat vrai au rang $s-1$.
  Comme $k\wedge n = 1$, on a $p_1 \dots p_{s-1} \wedge n = 1$, donc
  d'après l'hypothèse de récurrence, $F_1(\xi^{p_1 \dots p_{s-1}}) = 0.$
  Or $p_s \wedge n = 1$ donc \[ F_1[(\xi^{p_1 \dots p_{s-1}})^{p_s}] =
  F_1(\xi^k) = 0 \] ce qui achève le raisonnement par récurrence.

  Pour tout nombre entier $k$ premier avec $n$, on a donc $F_1(\xi^k) =
  0$. Or $\xi \in \Pi_n$ donc $\Pi_n = \{ \xi^k \mid k \wedge n = 1 \}$.
  Tous les éléments de $\Pi_n$ sont des rainces de $F_1$, ce qui prouve
  que $\Phi_n = F_1$ et que $\Phi_n$ est irréductible dans $\Q[X]$.
\end{proof}

\section*{Heuristique de la preuve}

L'idée de cette démonstration est de faire des allers-retours entre les
différents anneaux de polynômes, $\Z[X]$, $\Q[X]$ et $\C[X]$. On part du «
plus gros» pour montrer qu'on est à coefficient dans le «plus petit» et on
finit par un critère d'irréductibilité dans $\Q[X]$, ce qui fournit un
résultat intéressant sur les racines des ces polynômes.

La proposition 5 permet de construire de façon effective les polynômes
cyclotomiques à partir des racines $n$-ièmes de l'unité et des précédents.
La construction se fait par récurrence et c'est naturellement que le fait
que les polynômes cyclotomiques sont à coefficients entiers se démontre par
récurrence.

On commence par  montrer, à l'aide du lemme de Gauss que les polynômes
cyclotomiques peuvent s'écrire comme le produit de polynômes à coefficients
entiers irréductibles dans $\Q[X]$. On montre ensuite que ce produit est
réduit à un seul terme.

Pour cela, on étudie une racine complexe de $F_1$ et on va montrer qu'une de
ses puissances est racines d'un des polynômes $F_i$. Mais en fait, on peut
montrer que ce $F_i$ en question n'est autre que $F_1$. Finalement, on
montre que $\Phi_n$ n'est autre que $F_1$ et que ses racines sont
précisément les racines primitives de l'unité et donc $\Phi_n$ est bien
irréductible.

\end{document}
