\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}
\setlist[itemize,2]{label=$\blacktriangleright$}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\car}{\mathbf{1}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}


\title{Endomorphismes semi-simples}
\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}[Lemme des noyaux]\label{theo:lemme_noyaux}
  Soit $f \in \L(E)$ et $P = P_1\dots P_k \in \K[X]$, les polynômes $P_i$
  étant deux à deux premiers entre eux. Alors \[ \ker P(f) = \ker P_1(f)
  \oplus \cdots \oplus \ker P_k(f) . \]
\end{theoreme}

\begin{proposition}\label{prop:projection_polynome}
  Soit $f \in \L(E)$ et $F \in \K[X]$ un polynôme annulateur de $f$. Soit $F
  = \beta M_1^{\alpha_1}\dots M_s^{\alpha_s}$ la décomposition en facteurs
  irréductibles de $\K[X]$ du polynôme $F$. Pour tout $i \in \llbracket 1,
  s\rrbracket$, on note $N_i = \ker M_i^{\alpha_i}(f)$. O note alors $E =
  N_1 \oplus \cdots \oplus N_s$, et pour $i \in \llbracket 1, s\rrbracket$,
  la projection sur $N_i$ parallèlement à $\oplus_{j \neq i} N_j$ est un
  polynôme en $f$.
\end{proposition}

\begin{definition}
  On dit que $f \in \L(E)$ est \emph{semi-simple} si pour tout sous espace
  vectoriel $F$ de $E$ est stable par $f$, il existe un supplémentaire $S$
  de $F$ stable par $f$. Une matrice est dite \emph{semi-simple} si
  l'endomorphisme $f$ de $\L(E)$ dont $M$ est la matrice dans la base
  canonique de $\K^n$ est semi-simple.
\end{definition}

\begin{theoreme}\label{theo:caracterisation_semi-simple}
  Soit $f \in \L(E)$. $f$ est semi-simple si et seulement si son polynôme
  minimal est sans facteurs carrés.
\end{theoreme}

\begin{proof}
  Notons $\Pi_f$ le polynôme minimal de $f$. Soit $\\Pi_f =
  M_1^{\alpha_1}\dots M_s^{\alpha_s}$ la décomposition de $\Pi_f$ en
  facteurs irréductibles de $\K[X]$.
  \begin{itemize}
    \item Soit $F$ un sous-espace stable par $f$. Montrons que \[ F =
      \bigoplus_{i=1}^r \left[ \ker M_i^{\alpha_i}(f) \cap F \right] . \]
      Pour tout $i \in \llbracket 1, r \rrbracket$, on note $F_i = \ker
      M_i^{\alpha_i}(f)$. On sait, par le lemme des noyaux, que $E = F_1
      \oplus \cdots \oplus F_r$ ($P_i(f)$ est un polynôme annulateur de $f$
      donc $\ker P(f) = E$). Pour tout $i \in \llbracket 1, r \rrbracket$,
      on note $p_i$ la projection sur $F_i$ parallèlement à $\oplus_{j \neq
      i}F_j$. On sait que pour tout $i \in \llbracket 1, r \rrbracket$,
      $p_i$ est un polynôme en $f$.  Comme $F$ est stable par $f$, $F$ est
      donc stable par $p_i$, c'est-à-dire $p_i(F) \subset F$. On a aussi
      $p_i(F) \subset p_i(E) = F_i$. Finalement, on a $p_i(F) \subset F_i
      \cap F$ et $\Id_E = p_1 + \cdots + p_r$, on a \[ F \subset p_1(F) +
        \cdots + p_r(F) = p_1(F) \oplus \cdots \oplus p_r(F) \subset (F_1
      \cap F ) \oplus \cdots \oplus (F_r \cap F ) . \] L'inclusion
      réciproque est facile puisque pour tout $i \in \llbracket 1, r
      \rrbracket$, $F_i \cap F \subset F$ donc $(F_1 \cap F ) \oplus \cdots
      \oplus (F_r \cap F ) \subset F$.
    \item Supposons maintenant que $\Pi_f$ est irréductible. Montrons que
      $f$ est semi-simple.

      Soit $F$ un sous espace vectoriel stable par $f$. On veut donc montrer
      qu'il existe un supplémentaire $S$ de $F$ dans $E$ stable par $f$.

      Si $f = E$, alors c'est terminé en posant $S = \{0\}$.

      Sinon, soit $x_1 \in E \setminus F$. Considérons \[ E_{x_1} = \{
      P(f)(x_1), P\in\K[X] \} . \] Ce sous espace vectoriel $E_{x_1}$ est
      stable par $f$. Nous allons montrer que $E_{x_1} \cap F = \{0\}$.

      Soit $I_{x_1} = \{ P\in \K[X], P(f)(x_1) = 0 \}$. C'est un idéal de
      $\K[X]$, non réduit à zéro car $\Pi_(f) \in I_{x_1}$, donc il existe
      un polynôme unitaire $\Pi_{x_1}$ tel que $I_{x_1} = (\Pi_{x_1}) =
      \Pi_{x_1}\K[X]$. Comme $\Pi_f \in I_{x_1}$, le polynôme $\Pi_{x_1}$
      divise le polynôme $\Pi_f$ et $\Pi_f$ est irréductible et unitaire, on
      a $\Pi_{x_1} = \Pi_f$. Le polynôme $\Pi_{x_1}$ est donc irréductible.
      Soit $y \in E_{x_1} \cap F$. Il existe un polynôme $P$ tel que $y =
      P(f)(x_1)$. Si $ y\neq 0$, alors $P \notin I_{x_1} = (\Pi_{x_1})$ donc
      $\Pi_{x_1}$ ne divise pas $P$ et comme $\Pi_{x_1}$ irréductible, on a
      qu'ils sont premiers entre-eux. D'après le théorème de Bezout, il
      existe donc $U,V \in \K[X]$ tels que $UP + V\Pi_{x_1} = 1$, donc \[
        x_1 = U(f) \circ P(f)(x_1) + V(f) \circ \Pi_{x_1}(f)(x_1) = U(f)(y)
      . \] Or $y \in F$ et $F$ est stable par $f$, donx $x_1 = U(f)(y) \in
      F$, ce qui est absurde. On a donc $y =0$ et le résultat.

      On vient de montrer que $E_{x_1}$ et $F$ sont en somme directe et que
      $E_{x_1}$ est stable par $f$.

      Si $F \oplus E_{x_1} = E$, alors c'est terminé. Sinon on réitère le
      procédé et en nombre fini d'étape (on travaille en dimension finie),
      on construit $S = E_{x_1} \oplus \cdots \oplus E_{x_k}$ qui est stable
      par $f$ et qui vérifie $S \oplus F = E$.
    \item Montrons que $f$ est semi-simple si et seulement si $\Pi_f = M_1
      \dots M_r$ est produit de polynômes irréductibles unitaires de $\K[X]$
      deux à deux distincts.
      \begin{itemize}
        \item Supposons $f$ semi-simple. Soit $\Pi_f = M_1^{\alpha_1} \dots
          M_r^{\alpha_r}$ la décomposition de $\Pi_f$ en facteurs
          irréductibles. Il s'agit de montrer que pour tout $i \in
          \llbracket 1, r\rrbracket, \alpha_i = 1$. Supposons, au contraire
          qu'il existe un $i \in \llbracket 1, r \rrbracket$ tel que
          $\alpha_i \geqslant 2$. Si $M = M_i$, on voit qu'il existe $N \in
          \K[X]$ tel que $\Pi_f = M^2 N$. Soit $F = \ker M(f)$. Le sous
          espace vectoriel $F$ est stable par $f$ semi-simple donc il existe
          un supplémentaire $S$ de $F$ stable par $f$. Montrons que $MN(f)$
          s'annule sur $S$. Si $x \in S$, alors $MN(f)(x) \in F$ car $M(f)[
          MN(f)(x)] = \Pi_f(f)(x) = 0$ et $MN(f)(x) \in S$ car $S$ est
          stable par $f$. Il s'annule aussi sur $F$ car si $y \in F = \ker
          M(f)$, alors $MN(f)(y) = N[M(f)(y)] = 0$. Comme $F \oplus S = E$,
          $MN(f)$ s'annule sur $E$ tout entier, c'est-à-dire que $MN(f) =
          0$. Ceci contedit la minimalité du degré de du polynôme minimal
          $\Pi_f = M^2N$.
        \item Supposons que $\Pi_f = M_1 \dots M_r$ avec les $M_i$
          irréductibles unitaires et distincts deux à deux. Soit $F$ un sous
          espace vectoriel de $E$ stable par $f$. Pour tout $i \in
          \llbracket 1, r \rrbracket$, notons $F_1 = \ker M_i(f)$. On a $E =
          F_1 \oplus \cdots \oplus F_r$ et d'après ce qui précède, $F =
          \bigoplus_{i=1}^r (F \cap F_i)$. Pour tout $i \in \llbracket 1, r
          \rrbracket, F_i$ est stable par $f$. Notons $f_i \in \L(F_i)$ la
          restriction de $f$ à $F_i$. On a $M_i(f_i) = 0$ et $M_i$ est
          irréductible ce qui prouve que $M_i$ est un polynôme minimal de
          $f_i$. On utilise le deuxième point pour affirmer qu'alors $f_i$
          est semi-simple. Or $F \cap F_i$ est stable par $f_i$ donc il
          existe un supplémentaire $S_i$ stable par $f_i$  (et donc par
          $f$) tel que $(F_i \cap F ) \oplus S_i = F_i$. Posons $S = S_1
          \oplus \cdots \oplus S_r$, on a \begin{align*}
            E & = F_1 \oplus \cdots \oplus F_r                                                              \\
              & = \bigoplus_{i=1}^r (F_i \cap F) \oplus S_i                                                 \\
              & = \left[ \bigoplus_{i=1}^r (F_i \cap F) \right] \oplus \left[ \bigoplus_{i=1}^r S_i \right] \\
              & = F \oplus S                                                                                \\
          \end{align*} et $S$ est stable par $f$. L'endomorphisme est donc
          semi-simple.
      \end{itemize}
  \end{itemize}
\end{proof}


\end{document}
