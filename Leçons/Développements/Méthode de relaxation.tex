\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Méthode de relaxation}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}


\begin{theoreme}
  Soit $A \in \S_n^{++}(\R)$ ($n \geqslant 1$) et $b \in \R_n$. On note
  $(e_1, \dots, e_n)$ la base canonique de $\R^n$. On note $A = D + T +
  ^tT$ ($D$ diagonale, $T$ triangulaire supérieure stricte)

  Soit $\omega \in \R_+^*$ et $M = \frac{D}{\omega} + ^tT$, $N = \frac{1
  - \omega}{\omega}D + T$ de sorte que $A = M - N$.

  Alors $M$ est inversible et on peut définir par récurrence $u_{k+1} =
  M^{-1} (N u_k + b)$ pour tout $ k \geqslant 0$.

  De plus, si $\omega \in ]0;2[$, alors $u_k \xrightarrow{k\to +\infty} u
  \in \R^n$ tel que $Au = b$ et si $\omega > 2$, alors il existe $u_0
  \in \R^n$ tel que $(u_k)$ diverge.
\end{theoreme}

\section*{Preuve}

\subsection*{$D \in \S_n^{++}(\R)$}

$D$ est diagonale, donc $D \in \S_n(\R)$. $D = \left( \begin{matrix} d_1
& (0) \\ (0) & d_n \end{matrix} \right)$ et $\forall i \in \llbracket
1, n \rrbracket, d_i = ^te_i A e_i >0$ car $A \in \S_n^{++}(\R)$.
D'où $D \in \S_n^{++}(\R)$.

On en déduit que $M$ est inversible et que $(u_n)$ est bien définie.

\subsection*{}

Soit $u \in \R^n$ tel que $Au = b$ ($u$ existe et est unique car $A$ est
inversible).

On pose, pour tout $k \in \N$, $\varepsilon_k = u_k - u$.
\begin{align*}
  \forall k \in \N & \varepsilon_{k+1} & = u_{k+1} - u = M^{-1}(Nu_k +
  b) -u = M^{-1}(N\varepsilon_k + u) + M^{-1}b - u \\
  & & = M^{-1} N \varepsilon_k + M^{-1} [ ( M - A) u + b - Mu] = M^{-1}N
\varepsilon_k \end{align*}

Par récurrence immédiate, on a $\varepsilon_k = (M^{-1}N)^k
\varepsilon_0, \forall k \geqslant 0$

\subsection*{}

On suppose maintenant que $\omega \in ]0;2[$. Soit $\lVert \cdot{}
\rVert$ la norme subordonnée à $\lVert v \rVert = \sqrt{^t v A v}$
(norme d'algèbre)

Montrons que $\lVert M^{-1}N \rVert < 1$.

$\lVert M^{-1}N \rVert = \lvert \Id - M^{-1}A \rVert = \sup_{\lVert v
\rVert = 1}\lVert v - M^{-1}A v\rVert$.

Soit $v \in \R^n$ de norme 1 pour $\lVert \cdot \rVert$. Soit $w =
M^{-1}A v \neq 0$ ($Mw = Av$).

$\lVert v - w \rVert^2 = ^t(v - w)A(v - w) = \lVert v \rVert^2 - ^twAv -
^tv Aw + ^twAw$.

$\lVert v - w \rVert^2 = 1 - ^twMw -^tw ^tM w + ^tw (M - N)w = 1 - ^tw
(^tM + N)w$

Or $ (^tM + N) = \left( \frac{2}{\omega} - 1 \right) D \in
\S_n^{++}(\R)$ donc $^tw(^tM + N)w > 0$.

Ceci est vrai pour tout $w \in M^{-1}A(\S^1(\R^n))$ qui est compact par
continuité de $M^{-1}A$ donc $\exists \delta > 0, \forall \lVert w
\rVert = 1, w = M^{-1}A v \implies ^tw (^tM + N)w \geqslant \delta$ par
continuité de $w \mapsto ^tw (^tM + N)w$.

D'où $\lVert v - w \rVert^2 \leqslant 1 - \delta$ et en passant au
$\sup \ \ \lVert M^{-1}N \rVert < 1$. Donc $\varepsilon_k \to 0$ donc
$u_k \to u$.

\subsection*{}

Supposons maintenant que $\omega > 2$.

On note $\lambda_i$ les valeurs propres complexes de $M^{-1}N$ (non
nécessairement distinctes). On a $\prod_{i=1}^n \lambda_i =
\det(M^{-1}N) = (\det M)^{-1} \det(N) = \left( \prod_{i=1}^n
\frac{d_i}{\omega}\right)^{-1} \prod_{i=1}^n \frac{1-\omega}{\omega}d_i
= (1 - \omega)^n$.

$\prod_{i=1}^n \lvert \lambda_i \rvert = \lvert 1 - \omega \rvert^n > 1
\implies \exists j \in \llbracket 1, n\rrbracket \lvert \lambda_j \rvert
> 1$

Soit $e \in \C^n$ un vecteur propre associé à $\lambda_j, e = x + iy, x
\in \R^n, y \in \R^n$. $((M^{-1}N)^k e)_k$ diverge donc $((M^{-1}N)^k
x)_k$ ou $((M^{-1}N)^k y)_k$ diverge. On choisit alors $u_0 = x + u$ ou
$u_0 = y + u$

$u_1 = M^{-1} (N u_0 + b) = M^{-1}N x + M^{-1}N u + M^{-1} b = M^{-1}N x
+ u - M^{-1}A u + M^{-1} b$

$u_1 = M^{-1}N x + u$ et par récurrence, $u_k = (M^{-1}N)^k x + u$ donc
$(u_k)$ diverge. \qed

\end{document}
