\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}
\setlist[itemize,2]{label=$\blacktriangleright$}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\fcar}{\mathbf{1}}
\DeclareMathOperator{\car}{car}
\DeclareMathOperator{\card}{card}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Théorème de Sylow}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{lemme}
  Soit $G$ un groupe abélien fini. Soit $p$ un nombre premier divisant
  l'ordre de $G$.

  Alors il existe un sous-groupe de $G$ d'ordre $p$.
\end{lemme}
\begin{proof}
  $G$ étant fini, il existe un système de générateurs $(x_1, \dots, x_n)$.
  Notons $(r_1, \dots r_n)$ les ordres respectifs des générateurs.
  Considérons l'application \[ \varphi \colon \langle x_1 \rangle \times
    \cdots \times \langle x_n \rangle \to G, (y_1, \dots , y_n) \mapsto y_1
  \cdots y_n . \] $G$ étant abélien, $\varphi$ est un morphisme de groupes.
  De plus, $\varphi$ étant surjectif (puisque $(x_1, \dots, x_n)$ est un
  système de générateurs), $G$ est isomorphe à $(x_1, \dots, x_n)/\ker
  \varphi$. Donc $\card G \times \card \ker \varphi = \card ( \langle x_1
  \rangle \times \cdots \times \langle x_n \rangle ) = r_1 \cdots r_n$. Donc
  $\card G \mid r_1 \cdots r_n$, et donc $p \mid r_1 \cdots r_n$, donc il
  existe $i \in \llbracket 1, n \rrbracket, p \mid r_i$ Si $r_i = pq, q \in
  \N^*$, alors $x = x_i^q$ est d'ordre $p$ et $H = \langle x \rangle$ est
  un sous-groupe d'ordre $p$ de $G$.
\end{proof}


\begin{theoreme}[Théorème de Sylow]
  Soient $G$ un groupe abélien fini  d'ordre $h$ et $p$ un nombre premier.
  Si $p^{\alpha} \mid h$ avec $\alpha \in \N$, alors il existe un
  sous-groupe d'ordre $p^{\alpha}$ dans $G$.
\end{theoreme}

\begin{proof}
  On procède par récurrence sur $h = \card(G)$.
  \begin{itemize}
    \item Si $\card(G) = 1$, c'est évident.
    \item Sinon, supposons le résultat vrai pour les groupes d'ordre $< h =
      \card(G)$. On peut donc trouver une famille finie $(H_i)_{i\in I}$ de
      sous-groupes stricts de $G$ telle que \[ h = \card(G) = \card(Z(G)) +
      \sum_{i\in I} \frac{h}{\card(H_i)} \] où $Z(G)$ désigne le centre du
      groupe $G$. Deux cas se présentent alors :
      \begin{itemize}
        \item Il existe $i \in I$ tel que $p^{\alpha} \mid \card H_i$. Comme
          $\card H_i < \card G$, d'après l'hypothèse de récurrence, il
          existe un sous-groupe $H$ de $H_i$ d'ordre $p^{\alpha}$ et ainsi
          ce $H$ est bien un sous-groupe de $G$ d'ordre $p^{\alpha}$.
        \item Pour tout $i \in I, p^{\alpha} \nmid \card H_i$. Comme
          $p^{\alpha} \mid h, p \mid h / \card H_i$ pour tout $i \in I$.
          D'après l'équation aux classes, on a $p \mid \card Z(G)$ et $Z(G)$
          étant un groupe commutatif d'ordre $p$ d'après le lemme, il existe
          un sous-groupe $C$ d'ordre $p$ dans $Z(G)$. Comme $C \subset
          Z(G), C \triangleleft G$. Soit $\pi$ la surjection canonique de
          $G$ dans $G/C$. L'ordre du groupe quotient est $\card G / \card C
          = h/p < h = \card G$ et comme $p^{\alpha - 1} \mid \card G/C$ on
          sait, d'après l'hypothèse de récurrence qu'il existe un
          sous-groupe $H'$ de $G/C$ d'ordre $p^{\alpha - 1}$. Le sous-groupe
          $H = \pi^{-1}(H')$ est donc d'ordre $\card(C) \card(H') =
          p^{\alpha}$, d'où le résultat.
      \end{itemize}
  \end{itemize}
\end{proof}

\end{document}
