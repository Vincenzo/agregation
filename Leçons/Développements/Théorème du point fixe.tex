\documentclass[12pt,a4paper,french]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lmodern}
%\usepackage{kpfonts}

\usepackage{amsmath,amsfonts,amssymb,amsthm}
\usepackage{tikz}
\usetikzlibrary{calc,intersections,through,backgrounds}
\usepackage{tikz-cd}
%\usepackage{tkz-euclide}
%\usetkzobj{all}
\usepackage{wrapfig}
\usepackage{enumitem}
\usepackage{stmaryrd}
\usepackage{mdframed}

\theoremstyle{definition}
\newtheorem{definition}{Définition}
\theoremstyle{plain}
\newtheorem{theoreme}[definition]{Théorème}
\newtheorem{theoremedefinition}[definition]{Théorème et définition}
\newtheorem{proposition}[definition]{Proposition}
\newtheorem{corollaire}[definition]{Corollaire}
\newtheorem{lemme}[definition]{Lemme}
\newtheorem*{exemple}{Exemple}
\newtheorem*{exemples}{Exemples}
\newtheorem*{notation}{Notation}
\theoremstyle{remark}
\newtheorem*{rappel}{Rappel}
\newtheorem*{application}{Application}
\newtheorem*{remarque}{Remarque}
\newtheorem*{remarques}{Remarques}

\setlist[enumerate,1]{label=(\roman*)}
\setlist[itemize,1]{label=\textbullet}
\setlist[itemize,2]{label=$\blacktriangleright$}

\renewcommand{\k}{\mathbf{k}}
\newcommand{\R}{\mathbf{R}}
\newcommand{\N}{\mathbf{N}}
\newcommand{\K}{\mathbf{K}}
\newcommand{\Q}{\mathbf{Q}}
\newcommand{\Z}{\mathbf{Z}}
\newcommand{\U}{\mathbf{U}}
\newcommand{\C}{\mathbf{C}}
\newcommand{\F}{\mathbf{F}}
\newcommand{\doubleaccent}{\H}
\renewcommand{\H}{\mathbf{H}}
\newcommand{\Fe}{\mathcal{F}}
\newcommand{\E}{\mathbf{E}}
\newcommand{\slcr}{\P}
\renewcommand{\P}{\mathbf{P}}
\newcommand{\Co}{\mathcal{C}}
\newcommand{\Lc}{\mathbf{L}}
\renewcommand{\L}{\mathcal{L}}
\newcommand{\Normal}{\mathcal{N}}
\newcommand{\B}{\mathcal{B}}
\newcommand{\parm}{\S}
\renewcommand{\S}{\mathcal{S}}
\newcommand{\M}{\mathcal{M}}
\newcommand{\T}{\mathcal{T}}
\newcommand{\textempty}{\O}
\renewcommand{\O}{\mathcal{O}}
\newcommand{\Sp}{\mathop{Sp}}
\newcommand{\im}{\mathop{Im}}
\newcommand{\Id}{\mathop{Id}\nolimits}
\newcommand{\Card}{\mathop{Card}}
\newcommand{\Aut}{\mathop{Aut}}
\newcommand{\Var}{\mathop{Var}}
\newcommand{\GL}{\mathop{GL}\nolimits}
\newcommand{\SO}{\mathop{SO}\nolimits}
\newcommand{\diff}{\mathop{}\!\mathrm{d}}
\newcommand{\Vect}{\mathop{Vect}\nolimits}
\newcommand{\fcar}{\mathbf{1}}
\DeclareMathOperator{\car}{car}
\DeclareMathOperator{\card}{card}

\parindent0pt

\usepackage{babel}
\usepackage{draftwatermark}

\everymath{\displaystyle}

\title{Théorème du point fixe}

\author{Vincent-Xavier \bsc{Jumel}}
\date{\today}

\begin{document}
\maketitle

\SetWatermarkText{Brouillon}

\section*{Leçons}

\section*{Énoncé}

\begin{theoreme}
  Soient $X$ un espace métrique complet (non vide), $d$ la distance de $X$,
  et $F$ une application de $X$ dans lui-même. On suppose que $F$ est
  contractante, c'està dire qu'il existe une constante positive $k$,
  strictement inférieure à 1, telle que \[ d(F(x),F(y)) \leqslant k d(x,y)
  \] pour tous $x,y \in X$.

  Alors il existe un unique point $a \in X$ tel que $F(a) = a$ (point fixe
    de $F$). De plus, ce point peut s'obtenir comme limite de la suite
    $(x_n)_{n\geqslant 0}$ des itérés définie par récurrence à partir d'un
    point quelconque $x_0$ de $X$ selon $x_{n+1} = F(x_n)$. On a plus
    précisément, pour $n \geqslant 1$, \[ d(x_n,a) \leqslant \frac{k^n} {1 -
    k}d(x_0,x_1) . \]
\end{theoreme}

\section*{Preuve}

\begin{proof}
  Pour $n \geqslant 1$, on a \[ d(x_{n+1},x_n) = d(F(x_n),F(x_{n-1}))
    \leqslant k d(x_n,x_{n-1}), \] et on en déduit $d(x_{n+1},x_n) \leqslant
    k^n d(x_1,x_0)$. En utilisant l'inégalité triangulaire, on a \[
      d(x_n,x_{n+p}) \leqslant (k^n + \cdots + k^{n+p - 1})d(x_0,x_1)
    \leqslant \frac{k^n}{1 - k}d(x_0,x_1) \] en sommant une suite
    géométrique de raison $ k < 1$.

    Pourn $n$ assez grand, et $p \geqslant 1$, on a $d(x_n,x_{n+p})
    \leqslant \varepsilon$ et donc les $x_n$ forment une suite de Cauchy qui
    converge vers $a$ dans $X$ puisque celui-ci est complet. Par passage à
    la limite en $p$, on obtient \[ d(x_n,a) \leqslant \frac{k^n}{1-k}
    d(x_0,x_1) . \]

    Enfin, $x_{n+1} = F(x_n)$ tend vers $a$ et vers $F(a)$ et $F$ est
    continue car contractante, d'où $F(a) = a$ et $a$ est un point fixe de
    $F$.

    S'il y a vait un autre point fixe $b$, on aurait \[ d(a,b) =
    d(F(a),F(b)) \leqslant k d(a,b) \] d'où $d(a,b) = 0$ et donc $a = b$.
\end{proof}

\begin{proposition}
  Soit $X$ un espace métrique complet et $F$ une application de $X$ dans
  lui-même. On suppose qu'une certaine itérée de $F^p$ est contractante avec
  $p \geqslant 1$ un entier.

  Alors $F$ possède encore un unique point fixe et c'est la limite de la
  suite $(F^n(x_0))_{n\geqslant 0}$ avec $x_0$ quelconque.
\end{proposition}

\begin{proof}
  En utilisant le théorème précédent, on a $a$ qui est l'unique point fixe
  de $F^p$ et limite de la suite des $(F^{np}(x_0))_{n \geqslant 0}$ pour
  tout $x_0 \in X$. Comme \[ F(a) = F(F^p(a)) = F^p(F(a)) , \] $F(a)$ est
  aussi un point fixe de $F^p$, d'où $F(a) = a$ par unicité du point fixe de
  $F^p$ et donc $a$ est aussi un point fixe de $F$. Inversement, tout point
  fixe de $F$ est aussi un point fixe de $F^p$, donc $a$ est l'unique point
  fixe de $F$.

  Soit $k_p$ la constante de Lipschitz de $F^p$ ; d'après le théorème 1, on
  a \[ d(F^{np}(x_0),a ) \leqslant \frac{k_p^n}{1 - k_p} d(x_0, F(x_0)) . \]
  Puisque c'est vrai pour tout $x_0 \in X$, on peut prendre $F^q(x_0)$ et on
  obtient \[ d(F^{np + q}(x_0),a ) \leqslant \frac{k_p^n}{1 - k_p}
  d(F^q(x_0), F^{q+p}(x_0)) . \] $np + q$ est l'écriture de la division
  euclidienne d'un certain $m$ par $p$ pour peu que $0 \leqslant q < p$,
  d'où pour $n > (m/p) - 1$ on obtient \[ d(F^m(x_0), a) \leqslant \frac{C
  k_p^{m/p}}{1 - k_p}, \quad m\in N \] où $C$ est une constante positive
  indépendante de $m$.

  Ainsi, on a que $(F^n(x_0))_{n\geqslant 0}$ converge «géométriquement»
  vers $a$.
\end{proof}

\end{document}
